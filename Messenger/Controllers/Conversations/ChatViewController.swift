//
//  ChatViewController.swift
//  Messenger
//
//  Created by Binaya on 08/07/2022.
//

import UIKit
import MessageKit
import InputBarAccessoryView //Brought in by MessageKit
import SDWebImage
import AVKit
import CoreLocation

public enum MediaContentType {
    
    case photo
    case video
    
}

/// A view controller that handles the user's chat processes.
final class ChatViewController: MessagesViewController {

    // MARK: - Properties
    public var isNewConversation = false
    public let otherUserEmail: String
    private var conversation: Conversation?
    private var messages = [Message]()
    private var sender: Sender? {
        guard let senderEmail = UserDefaults.standard.value(forKey: GlobalConstants.UserDefaultKeys.emailKey) as? String,
              let senderName = UserDefaults.standard.value(forKey: GlobalConstants.UserDefaultKeys.nameKey) as? String else {
                  print("Email and name are nil and they should be cached in the user defaults.")
                  return nil
              }
        let safeEmail = DatabaseManager.getSafeEmail(with: senderEmail)
        return Sender(photoURL: "",
                      senderId: safeEmail,
                      displayName: senderName)
    }
    private var loggedInUserPhotoUrl: URL?
    private var recipientPhotoUrl: URL?
    
    // MARK: - Static properties
    public static let dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateStyle = .medium
        formatter.timeStyle = .long
        formatter.locale = Locale(identifier: "en_US")
        return formatter
    }()
    
    // MARK: - Initializers
    init(otherUserEmail: String, conversation: Conversation?) {
        self.otherUserEmail = otherUserEmail
        self.conversation = conversation
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Lifecycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        messageInputBar.inputTextView.becomeFirstResponder()
    }
        
    // MARK: - Methods
    private func setup() {
        setupMessagesCollectionView()
        setupInputBarButtonItem()
        setupUsersPhotoUrls()
        if let conversation = conversation {
            startListeneningForConversationMessages(with: conversation.id, shouldScrollToBottom: true)
        }
    }
    
    private func setupUsersPhotoUrls() {
        setupLoggedInUserPhotoUrl()
        setupRecipientPhotoUrl()
    }
    
    private func setupLoggedInUserPhotoUrl() {
        if let loggedinUserPhotoUrlString = UserDefaults.standard.value(forKey: GlobalConstants.UserDefaultKeys.profilePictureDownloadUrlStringKey) as? String, let loggedInUserPhotoUrl = URL(string: loggedinUserPhotoUrlString) {
            self.loggedInUserPhotoUrl = loggedInUserPhotoUrl
        } else {
            //Photo download url is nil in the user defaults.
            guard let loggedInUserEmail = UserDefaults.standard.value(forKey: GlobalConstants.UserDefaultKeys.emailKey) as? String else { return }
            let loggedInUserImagePath = getImagePath(for: loggedInUserEmail)
            StorageManager.shared.getProfilePictureDownloadUrl(for: loggedInUserImagePath) { [weak self] result in
                switch result {
                case.success(let loggedInUserPhotoUrl):
                    self?.loggedInUserPhotoUrl = loggedInUserPhotoUrl
                    DispatchQueue.main.async {
                        self?.messagesCollectionView.reloadData()
                    }
                case.failure(let error):
                    print("Failed to get the download url for the logged in user's profile picture: \(error)")
                }
            }
        }
    }
    
    private func setupRecipientPhotoUrl() {
        let recipientImagePath = getImagePath(for: otherUserEmail)
        StorageManager.shared.getProfilePictureDownloadUrl(for: recipientImagePath) { [weak self] result in
            switch result {
            case.success(let recipientPhotoUrl):
                self?.recipientPhotoUrl = recipientPhotoUrl
                DispatchQueue.main.async {
                    self?.messagesCollectionView.reloadData()
                }
            case.failure(let error):
                print("Failed to get the download url for the recipient's profile picture: \(error)")
            }
        }
    }
    
    private func getImagePath(for email: String) -> String {
        let safeEmail = DatabaseManager.getSafeEmail(with: email)
        let safeEmailImageFileName = safeEmail + "_profile_picture.png"
        let imagePath = "images/" + safeEmailImageFileName
        return imagePath
    }
    
    private func setupMessagesCollectionView() {
        messagesCollectionView.messagesDataSource = self
        messagesCollectionView.messagesLayoutDelegate = self
        messagesCollectionView.messagesDisplayDelegate = self
        messagesCollectionView.messageCellDelegate = self
    }
    
    private func startListeneningForConversationMessages(with conversationId: String, shouldScrollToBottom: Bool) {
        DatabaseManager.shared.getAllMessagesForAConversation(with: conversationId) { [weak self] result in
            switch result {
            case .success(let messages):
                guard !messages.isEmpty else {
                    print("Messages collection is empty!")
                    return
                }
                self?.filterOldMessagesForNewConvo(with: messages, shouldScrollToBottom: shouldScrollToBottom)
            case .failure(let error):
                self?.alertWithOKActionCancel(message: error.localizedDescription)
                print("ERROR in startListeneningForConversationMessages: \(error)")
            }
        }
    }
    
    private func filterOldMessagesForNewConvo(with messages: [Message], shouldScrollToBottom: Bool) {
//        guard let convoCreationDateString = conversation?.creationDate,
//                let convoCreationDate = ChatViewController.dateFormatter.date(from: convoCreationDateString) else { return }
//        self.messages = messages.compactMap({ convoCreationDate < $0.sentDate ? $0 : nil })
        self.messages = messages
        reloadMessagesCollectionView(shouldScrollToBottom: shouldScrollToBottom)
    }
    
    private func reloadMessagesCollectionView(shouldScrollToBottom: Bool) {
        DispatchQueue.main.async { [weak self] in
            //Use reloadDataAndKeepOffset() instead of reloadData() as the update of a new message addition will happen in real time. So, if a user has scrolled to the top and is reading older messages and a new message comes in, we do not want the messages collection view to scroll down to the new message which is a bad user experience. This method will add a new message to the bottom without scrolling the messages collection view to the bottom automatically.
            self?.messagesCollectionView.reloadDataAndKeepOffset()
            if shouldScrollToBottom {
                self?.messagesCollectionView.reloadData()
            }
        }
    }
    
    /// Adds a "+" image on the input bar button which is the area on the left of the messaging area.
    private func setupInputBarButtonItem() {
        messageInputBar.delegate = self
        let button = InputBarButtonItem()
        button.setSize(CGSize(width: 35, height: 35), animated: false)
        button.setImage(UIImage(systemName: "paperclip"), for: .normal)
        button.onTouchUpInside { [weak self] _ in
            self?.presentInputBarButtonActionSheet()
        }
        messageInputBar.setLeftStackViewWidthConstant(to: 36, animated: false)
        messageInputBar.setStackViewItems([button], forStack: .left, animated: false)
    }
    
    private func presentInputBarButtonActionSheet() {
        let actionSheet = UIAlertController(title: "Attach Media",
                                            message: "What would you like to attach?",
                                            preferredStyle: .actionSheet)
        actionSheet.addAction(UIAlertAction(title: "Photo",
                                            style: .default,
                                            handler: { [weak self] _ in
            self?.presentPhotoInputActionSheet()
        }))
        actionSheet.addAction(UIAlertAction(title: "Video",
                                            style: .default,
                                            handler: { [weak self] _ in
            self?.presentVideoInputActionSheet()
        }))
        actionSheet.addAction(UIAlertAction(title: "Audio",
                                            style: .default,
                                            handler: { [weak self] _ in
            
        }))
        actionSheet.addAction(UIAlertAction(title: "Location",
                                            style: .default,
                                            handler: { [weak self] _ in
            self?.pushLocationPickerViewController()
        }))
        actionSheet.addAction(UIAlertAction(title: "Cancel",
                                            style: .cancel,
                                            handler: nil))
        present(actionSheet, animated: true)
    }
    
    private func presentPhotoInputActionSheet() {
        let actionSheet = UIAlertController(title: "Attach Photo",
                                            message: "Where would you like to attach a photo from?",
                                            preferredStyle: .actionSheet)
        actionSheet.addAction(UIAlertAction(title: "Camera",
                                            style: .default,
                                            handler: { [weak self] _ in
            self?.presentImagePickerController(for: .camera, contentType: .photo)
        }))
        actionSheet.addAction(UIAlertAction(title: "Photo Library",
                                            style: .default,
                                            handler: { [weak self] _ in
            self?.presentImagePickerController(for: .photoLibrary, contentType: .photo)
        }))
        actionSheet.addAction(UIAlertAction(title: "Cancel",
                                            style: .cancel,
                                            handler: nil))
        present(actionSheet, animated: true)
    }
    
    private func presentVideoInputActionSheet() {
        let actionSheet = UIAlertController(title: "Attach Video",
                                            message: "Where would you like to attach a video from?",
                                            preferredStyle: .actionSheet)
        actionSheet.addAction(UIAlertAction(title: "Camera",
                                            style: .default,
                                            handler: { [weak self] _ in
            self?.presentImagePickerController(for: .camera, contentType: .video)
        }))
        actionSheet.addAction(UIAlertAction(title: "Photo Library",
                                            style: .default,
                                            handler: { [weak self] _ in
            self?.presentImagePickerController(for: .photoLibrary, contentType: .video)
        }))
        actionSheet.addAction(UIAlertAction(title: "Cancel",
                                            style: .cancel,
                                            handler: nil))
        present(actionSheet, animated: true)
    }

    private func presentImagePickerController(for sourceType: UIImagePickerController.SourceType, contentType: MediaContentType) {
        let picker = UIImagePickerController()
        picker.sourceType = sourceType
        picker.delegate = self
        picker.allowsEditing = true
        if contentType == .video {
            picker.mediaTypes = ["public.movie"] //Limit the user to only select videos.
            //The actual video quality size is quite large and we do not have enough space in firebase free storage.
            picker.videoQuality = .typeMedium
        }
        present(picker, animated: true)
    }
    
    // MARK: - Location message upload section
    private func pushLocationPickerViewController(with locationCoordinates: CLLocationCoordinate2D? = nil) {
        let locationPickerViewController = LocationPickerViewController(locationCoordinates: locationCoordinates)
        locationPickerViewController.title = locationCoordinates == nil ? "Pick a Location" : "Location"
        if locationCoordinates == nil {
            locationPickerViewController.locationPicked = { [weak self] userPickedLocation2DCoordinates in
                self?.sendALocationMessage(with: CLLocation(latitude: userPickedLocation2DCoordinates.latitude,
                                                            longitude: userPickedLocation2DCoordinates.longitude))
            }
        }
        navigationController?.pushViewController(locationPickerViewController, animated: true)
    }
    
    private func sendALocationMessage(with userCLLocation: CLLocation) {
        guard let messageId = getMessageId(),
              let sender = sender else { return }
        let location = Location(location: userCLLocation,
                                size: .zero) //Sending 0 for size as it is irrelevant for now as we will change it at run-time.
        let message = Message(sender: sender,
                              messageId: messageId,
                              sentDate: Date(),
                              kind: .location(location))
        updateChatWith(message)
    }
    
}

// MARK: - MessagesDataSource extension
extension ChatViewController: MessagesDataSource {
    
    //The MessagesDataSource determines where(left or right) to places the message bubble based on the current user. So, the Message object's sender property's senderId will be compared to the sender's senderId this currentSender() method will return.
    func currentSender() -> SenderType {
        if let sender = sender {
            return sender
        }
        fatalError("Self sender is nil, Email should be cached!")
    }
    
    func numberOfSections(in messagesCollectionView: MessagesCollectionView) -> Int {
        return messages.count
    }
    
    func messageForItem(at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> MessageType {
        //MessageKit framework uses sections to seperate every single messages.
        return messages[indexPath.section]
    }
    
}

// MARK: - MessagesLayoutDelegate extension
extension ChatViewController: MessagesLayoutDelegate {
    
}

// MARK: - MessageCellDelegate extension
extension ChatViewController: MessageCellDelegate {
    
    func didTapMessage(in cell: MessageCollectionViewCell) {
        guard let indexPath = messagesCollectionView.indexPath(for: cell) else { return }
        let message = messages[indexPath.section]
        switch message.kind {
        case .location(let location):
            let coordinates = location.location.coordinate
            pushLocationPickerViewController(with: coordinates)
        default:
            break
        }
    }
    
    func didTapImage(in cell: MessageCollectionViewCell) {
        guard let indexPath = messagesCollectionView.indexPath(for: cell) else { return }
        //MessageKit uses a section per message.
        let message = messages[indexPath.section]
        switch message.kind {
        case .photo(let media):
            guard let imageUrl = media.url else { return }
            let vc = PhotoViewerViewController(with: imageUrl)
            vc.modalPresentationStyle = .fullScreen
            present(vc, animated: true)
        case .video(let media):
            //NOTE: To play the video, we have to write the code inside of this didTapImage() method as the message cell gets rendered as a photo with a play image in the middle.
            guard let videoUrl = media.url else { return }
            let vc = AVPlayerViewController()
            vc.allowsPictureInPicturePlayback = true
            vc.canStartPictureInPictureAutomaticallyFromInline = true
            vc.player = AVPlayer(url: videoUrl)
            present(vc, animated: true) {
                vc.player?.play()
            }
        default:
            break
        }
    }
    
}

// MARK: - MessagesDisplayDelegate extension
extension ChatViewController: MessagesDisplayDelegate {
    
    //This method is only called when the message kind is of type photo.
    func configureMediaMessageImageView(_ imageView: UIImageView, for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) {
        guard let message = message as? Message else { return }
        switch message.kind {
        case .photo(let media):
            guard let imageUrl = media.url else { return }
            imageView.sd_setImage(with: imageUrl,
                                  placeholderImage: UIImage(systemName: "photo"),
                                  options: [],
                                  completed: nil)
        default:
            break
        }
    }
    
    func backgroundColor(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> UIColor {
        let sender = message.sender
        //SenderId is the user's safe email.
        if let loggedInUserSenderId = self.sender?.senderId, sender.senderId == loggedInUserSenderId {
            //Logged in user messages
            return UIColor(named: "MyMessageColor") ?? UIColor(red: 0.00, green: 0.71, blue: 1.00, alpha: 1.00)
        }
        //Recipient messages
        return UIColor(named: "RecipientMessageColor") ?? UIColor(red: 0.91, green: 0.91, blue: 0.91, alpha: 1.00)
    }
    
    func textColor(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> UIColor {
        let sender = message.sender
        //SenderId is the user's safe email.
        if let loggedInUserSenderId = self.sender?.senderId, sender.senderId == loggedInUserSenderId {
            //Logged in user text messages
            return .systemBackground
        }
        //Recipient text messages
        return .label
    }
    
    func configureAvatarView(_ avatarView: AvatarView, for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) {
        let sender = message.sender
        //SenderId is the user's safe email.
        if let loggedInUserSenderId = self.sender?.senderId, sender.senderId == loggedInUserSenderId {
            //Logged in user
            if let loggedInUserPhotoUrl = loggedInUserPhotoUrl {
                avatarView.sd_setImage(with: loggedInUserPhotoUrl, completed: nil)
            } else {
                //Fetch user photo url
                setupLoggedInUserPhotoUrl()
            }
        } else {
            //Recipient
            if let recipientPhotoUrl = recipientPhotoUrl {
                avatarView.sd_setImage(with: recipientPhotoUrl, completed: nil)
            } else {
                //Fetch recipient photo url
                setupRecipientPhotoUrl()
            }
        }
    }
    
}

// MARK: - InputBarAccessoryViewDelegate extension
extension ChatViewController: InputBarAccessoryViewDelegate {
    
    func inputBar(_ inputBar: InputBarAccessoryView, didPressSendButtonWith text: String) {
        guard !text.replacingOccurrences(of: " ", with: "").isEmpty,
              let sender = sender,
              let messageId = getMessageId() else {
                  print("Error: User sent an empty text message and sender object is nil.")
                  alertWithOKActionCancel(message: "Error: User sent an empty text message and sender object is nil.")
                  return
              }
        inputBar.inputTextView.text = ""
        print("Sender's message: \(text)")
        let message = Message(sender: sender,
                              messageId: messageId, //Each message have to be unique.
                              sentDate: Date(),
                              kind: .text(text))
        updateChatWith(message)
    }
    
    ///Updates the chat by either create a new conversation or appending to an existing conversation.
    private func updateChatWith(_ message: Message) {
        if isNewConversation {
            //Create a new convo in the database
            createANewConvo(with: message)
        } else {
            //Append the text message to the existing conversation.
            updateAnExistingConvo(with: message) { messageAppendedToExistingConversation in
                if messageAppendedToExistingConversation {
                    print("New message appended to the conversation.")
                } else {
                    print("Failed to send a new message to the conversation.")
                }
            }
        }
    }
    
    /// Creates a new conversation with the other user.
    private func createANewConvo(with firstTextMessage: Message) {
        DatabaseManager.shared.createANewConversation(with: otherUserEmail,
                                                      recipientName: title ?? "User",
                                                      firstTextMessage: firstTextMessage) { [weak self] conversation in
            if let conversation = conversation {
                print("Created a new convo with the user \(self?.otherUserEmail ?? "") with the newly sent message.")
                self?.isNewConversation = false
                self?.conversation = conversation
                NotificationCenter.default.post(name: .firstConversationCreated, object: nil)
                self?.startListeneningForConversationMessages(with: conversation.id, shouldScrollToBottom: true)
            } else {
                print("Failed to create a new convo with the user \(self?.otherUserEmail ?? "") with the newly sent message.")
            }
        }
    }
    
    /// Appends a new message to an existing conversation.
    private func updateAnExistingConvo(with newMessage: Message, completion: @escaping (Bool) -> Void) {
        guard let conversation = conversation, let recipientName = title else {
            print("Error in inputBar: Attempting to append a new message to an existing conversation when conversation ID is nil!")
            alertWithOKActionCancel(message: "Error in inputBar: Attempting to append a new message to an existing conversation when conversation ID is nil!")
            return
        }
        DatabaseManager.shared.appendMessageTo(conversationId: conversation.id,
                                               newMessage: newMessage,
                                               recipientEmail: otherUserEmail,
                                               recipientName: recipientName,
                                               completion: completion)
    }

    /// Creates a unique id for each message using otherUserEmail, senderEmail, and date.
    private func getMessageId() -> String? {
        //For example: senderEmail_otherUserEmail_date_randomInt
        //NOTE: We are using the "Self" with a capital "S" because we are referring to a static property.
        guard let senderEmail = UserDefaults.standard.value(forKey: GlobalConstants.UserDefaultKeys.emailKey) as? String else {
            print("Email is nil in user defaults!")
            return nil
        }
        let safeSenderEmail = DatabaseManager.getSafeEmail(with: senderEmail)
        let dateString = ChatViewController.dateFormatter.string(from: Date())
        let messageId = "\(otherUserEmail)_\(safeSenderEmail)_\(dateString)"
        print("Created a unique message id: \(messageId )")
        return messageId
    }
    
}

// MARK: - UIImagePickerControllerDelegate & UINavigationControllerDelegate extensions
extension ChatViewController: UIImagePickerControllerDelegate & UINavigationControllerDelegate {
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true, completion: nil)
        if let image = info[.editedImage] as? UIImage, let imageData = image.pngData() {
            //Upload photo message
            uploadImageDataToFirebase(with: imageData)
        } else if let videoUrl = info[.mediaURL] as? URL {
            //Upload video message: The reason we upload an URL instead of Data is because the data size for a video is quite large. So it is better to use URL for better performance and to avoid crashes.
            uploadVideoURLToFirebase(with: videoUrl)
        }
    }
    
    // MARK: - Photo message upload section
    private func uploadImageDataToFirebase(with imageData: Data) {
        guard let messageId = getMessageId() else {
            print("Error in uploadImageDataToFirebase: Message Id return from getMessageId() method is nil!")
            alertWithOKActionCancel(message: "Error in uploadImageDataToFirebase: Message Id return from getMessageId() method is nil!")
            return
        }
        let fileName = "photo_message_" + messageId.replacingOccurrences(of: " ", with: "-") + ".png"
        StorageManager.shared.uploadMessagePhoto(with: imageData, fileName: fileName) { [weak self] result in
            switch result {
            case .success(let photoDownloadUrlString):
                print("Uploaded photo message url string: \(photoDownloadUrlString)")
                //Send the photo message.
                self?.sendImageMessageToRecipient(with: photoDownloadUrlString, and: messageId)
            case .failure(let error):
                print("Error in uploadImageDataToFirebase: \(error)")
                self?.alertWithOKActionCancel(message: "Error in uploadImageDataToFirebase: \(error.localizedDescription)")
            }
        }
    } 
    
    private func sendImageMessageToRecipient(with imageUrlString: String, and messageId: String) {
        guard let sender = sender,
              let imageUrl = URL(string: imageUrlString),
              let placeholderImage = UIImage(systemName: "photo") else {
                  print("Error in sendImageMessageToRecipient: The conversationId, sender, VC title(Contains the other user name), image url, and placeholderImage are nil!")
                  alertWithOKActionCancel(message: "Error in sendImageMessageToRecipient: The conversationId, sender, VC title(Contains the other user name), image url, and placeholderImage are nil!")
                  return
              }
        //The image rendering size is sent as zero as we do not need a size when we are persisting it in the Database. When we get the messages while listening, then we will provde a rendering constant size.
        let media = Media(url: imageUrl,
                          image: nil,
                          placeholderImage: placeholderImage,
                          size: .zero)
        let message = Message(sender: sender,
                              messageId: messageId,
                              sentDate: Date(),
                              kind: .photo(media))
        updateChatWith(message)
    }
    
    // MARK: - Video message upload section
    private func uploadVideoURLToFirebase(with videoUrl: URL) {
        guard let messageId = getMessageId() else {
            print("Error in uploadVideoURLToFirebase: Message Id return from getMessageId() method is nil!")
            alertWithOKActionCancel(message: "Error in uploadVideoURLToFirebase: Message Id return from getMessageId() method is nil!")
            return
        }
        let fileName = "video_message_" + messageId.replacingOccurrences(of: " ", with: "-") + ".mov"
        StorageManager.shared.uploadMessageVideo(with: videoUrl, fileName: fileName) { [weak self] result in
            switch result {
            case .success(let videoDownloadUrlString):
                print("Uploaded video message url string: \(videoDownloadUrlString)")
                //Send the video message.
                self?.sendVideoMessageToRecipient(with: videoDownloadUrlString, and: messageId)
            case .failure(let error):
                self?.alertWithOKActionCancel(message: "Error in uploadVideoURLToFirebase: \(error.localizedDescription)")
                print("Error in uploadVideoURLToFirebase: \(error)")
            }
        }
    }
    
    private func sendVideoMessageToRecipient(with videoUrlString: String, and messageId: String) {
        guard let sender = sender,
              let videoUrl = URL(string: videoUrlString),
              let placeholderImage = UIImage(systemName: "video") else {
                  alertWithOKActionCancel(message: "Error in sendVideoMessageToRecipient: The conversationId, sender, VC title(Contains the other user name), video url, and placeholderImage are nil!")
                  print("Error in sendVideoMessageToRecipient: The conversationId, sender, VC title(Contains the other user name), video url, and placeholderImage are nil!")
                  return
              }
        //The video rendering size is sent as zero as we do not need a size when we are persisting it in the Database. When we get the messages while listening, then we will provde a rendering constant size.
        let media = Media(url: videoUrl,
                          image: nil,
                          placeholderImage: placeholderImage,
                          size: .zero)
        let message = Message(sender: sender,
                              messageId: messageId,
                              sentDate: Date(),
                              kind: .video(media))
        updateChatWith(message)
    }
    
}
