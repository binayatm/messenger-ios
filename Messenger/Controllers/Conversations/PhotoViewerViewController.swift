//
//  PhotoViewerViewController.swift
//  Messenger
//
//  Created by Binaya on 20/06/2022.
//

import UIKit
import SDWebImage

/// A view controller that handles the displaying of a photo message.
final class PhotoViewerViewController: UIViewController {

    // MARK: - Properties
    private let url: URL
    private let imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    private let closeButton: UIButton = {
        let button = UIButton()
        button.tintColor = .white
        button.setImage(UIImage(systemName: "xmark"), for: .normal)
        return button
    }()
            
    // MARK: - Initializers
    init(with url: URL) {
        self.url = url
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Lifecycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .black
        view.addSubview(imageView)
        view.addSubview(closeButton)
        imageView.sd_setImage(with: url,
                              placeholderImage: UIImage(named: "photo"),
                              options: [],
                              completed: nil)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        imageView.frame = view.bounds
        closeButton.frame = CGRect(x: 16, y: 28, width: 24, height: 24)
        closeButton.addTarget(self, action: #selector(closeButtonTapped), for: .touchUpInside)
    }

    // MARK: - @objc Methods
    @objc private func closeButtonTapped() {
        dismiss(animated: true)
    }
    
}
