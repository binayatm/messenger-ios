//
//  ViewController.swift
//  Messenger
//
//  Created by Binaya on 20/06/2022.
//

import UIKit
import FirebaseAuth
import JGProgressHUD

/// A view controller that handles all of the user's conversations processes.
final class ConversationsViewController: UIViewController {

    // MARK: - Properties
    private var conversations: [Conversation] = []
    private let tableView: UITableView = {
        let tableView = UITableView()
        tableView.isHidden = true
        tableView.register(ConversationTableViewCell.self,
                           forCellReuseIdentifier: ConversationTableViewCell.identifier)
        return tableView
    }()
    private let noConversationsLabel: UILabel = {
        let label = UILabel()
        label.text = "No conversations!"
        label.textAlignment = .center
        label.textColor = .systemGray
        label.font = .systemFont(ofSize: 21, weight: .medium)
        label.isHidden = true
        return label
    }()
    private let loadingSpinner = JGProgressHUD(style: .dark)
    private var loginObserver: NSObjectProtocol?
    private var firstConversationCreationObserver: NSObjectProtocol?
        
    // MARK: - Lifecycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        setupNavBar()
        setupSubViewsLayout()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        validateUserLoggedInStatus()
    }
    
    deinit {
        if let loginObserver = loginObserver {
            NotificationCenter.default.removeObserver(loginObserver)
        }
        if let firstConversationCreationObserver = firstConversationCreationObserver {
            NotificationCenter.default.removeObserver(firstConversationCreationObserver)
        }
        DatabaseManager.shared.removeAllObservers()
    }

    // MARK: - Methods
    private func setup() {
        setupSubViews()
        setupTableView()
        setupLoginObserver()
        startListeningForUserConversations()
    }
    
    private func setupNavBar() {
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .compose, target: self, action: #selector(didTapComposeBarButton))
    }
    
    /// Method that attaches a listener to the user's conversations array in the database to update the TableView every time a new conversation is added.
    private func startListeningForUserConversations() {
        DatabaseManager.shared.removeAllObservers()
        loadingSpinner.show(in: view)
        guard let loggedInUserEmail = UserDefaults.standard.value(forKey: GlobalConstants.UserDefaultKeys.emailKey) as? String else {
            loadingSpinner.dismiss(animated: true)
            print("Error in startListeningForUserConversations: User email is nil in UserDefaults.")
            conversations = []
            updateUI()
            return
        }
        let safeEmail = DatabaseManager.getSafeEmail(with: loggedInUserEmail)
        DatabaseManager.shared.getAllConversationsForUser(with: safeEmail) { [weak self] result in
            self?.loadingSpinner.dismiss(animated: true)
            switch result {
            case .success(let conversations):
                guard !conversations.isEmpty else {
                    print("Fetched conversations collection is empty!")
                    self?.conversations = []
                    self?.setupFirstConvoCreationObserver()
                    self?.updateUI()
                    return
                }
                print("Got all of the users conversations: \(conversations)")
                self?.conversations = conversations
                DispatchQueue.main.async {
                    self?.updateUI()
                }
            case .failure(let error):
                self?.conversations = []
                self?.updateUI()
                print("Error in startListeningForUserConversations: \(error)")
                self?.setupFirstConvoCreationObserver()
            }
        }
    }
    
    private func setupSubViews() {
        view.addSubview(tableView)
        view.addSubview(noConversationsLabel)
    }
    
    private func setupSubViewsLayout() {
        tableView.frame = view.bounds
        noConversationsLabel.frame = CGRect(x: view.width / 4,
                                           y: (view.height - 200) / 2,
                                           width: view.width / 2,
                                           height: 200)
    }
    
    private func validateUserLoggedInStatus() {
        if FirebaseAuth.Auth.auth().currentUser == nil {
            // Send to login page.
            let loginViewController = LoginViewController()
            let navigationController = UINavigationController(rootViewController: loginViewController)
            navigationController.modalPresentationStyle = .fullScreen
            present(navigationController, animated: false)
        } 
    }
    
    private func setupTableView() {
        tableView.dataSource = self
        tableView.delegate = self
        tableView.isHidden = false
    }
    
    private func setupLoginObserver() {
        loginObserver = NotificationCenter.default.addObserver(forName: .didCompleteLoggingIn, object: nil, queue: .main, using: { [weak self] _ in
            guard let strongSelf = self else { return }
            strongSelf.startListeningForUserConversations()
        })
    }
    
    //Convo gets created in the chat vc when the user sends a new message so when they come back to this page, we want to listen for the users conversation. We are doing this for the case where the user has 0 conversations and has just created a new one. For the case where the user already has other convos and he creates a new one, we will already be listening due to the listener method being called in the setup method.
    private func setupFirstConvoCreationObserver() {
        firstConversationCreationObserver = NotificationCenter.default.addObserver(forName: .firstConversationCreated, object: nil, queue: .main, using: { [weak self] _ in
            guard let strongSelf = self else { return }
            strongSelf.startListeningForUserConversations()
        })
    }
    
    private func updateUI() {
        if conversations.isEmpty {
            noConversationsLabel.isHidden = false
            tableView.isHidden = true
        } else {
            noConversationsLabel.isHidden = true
            tableView.isHidden = false
            tableView.reloadData()
        }
    }
    
    private func openAnExisting(conversation: Conversation) {
        let vc = ChatViewController(otherUserEmail: conversation.otherUserEmail, conversation: conversation)//Conversation id is passed.
        vc.title = conversation.otherUserName //Name of the person that the user was chatting with.
        vc.navigationItem.largeTitleDisplayMode = .never
        navigationController?.pushViewController(vc, animated: true)
    }
    
    private func createANewConversation(with recipient: SearchResultUser) {
        //Check if the currently logged in user already has a convo with the recipient user.
        DatabaseManager.shared.checkIfConversationAlreadyExists(with: recipient.email) { [weak self] result in
            guard let self = self else { return }
            switch result {
            case .success(let conversation):
                self.pushNewChatVC(with: recipient, conversation: conversation)
            case .failure(_):
                //Create a new convo as there is no existing convo between the recipient and the logged in user.
                //Email is also needed because it is the ID for our database in firebase.
                self.pushNewChatVC(with: recipient, conversation: nil)
            }
        }
    }
    
    private func pushNewChatVC(with recipient: SearchResultUser, conversation: Conversation?) {
        let vc = ChatViewController(otherUserEmail: recipient.email, conversation: conversation)
        vc.title = recipient.name
        vc.isNewConversation = conversation == nil ? true : false
        vc.navigationItem.largeTitleDisplayMode = .never
        navigationController?.pushViewController(vc, animated: true)
    }
    
    // MARK: - @objc methods
    @objc private func didTapComposeBarButton() {
        //Not creating a database entry when a user taps on a new user to start
        //a conversation so that we can save database space. Database entry is
        //only created when a message is sent.
        let vc = NewConversationViewController()
        vc.selectedAUserToChat = { [weak self] selectedUserToChat in
            guard let self = self else { return }
            let currentConvos = self.conversations
            if let existingConversation = currentConvos.first(where: { $0.otherUserEmail == DatabaseManager.getSafeEmail(with: selectedUserToChat.email) }) {
                self.openAnExisting(conversation: existingConversation)
            } else {
                self.createANewConversation(with: selectedUserToChat)  
            }
        }
        let navController = UINavigationController(rootViewController: vc)
        present(navController, animated: true)
    }
    
}

// MARK: - UITableViewDataSource extension
extension ConversationsViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return conversations.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ConversationTableViewCell.identifier,
                                                 for: indexPath) as! ConversationTableViewCell
        cell.configure(with: conversations[indexPath.row])
        return cell
    }
    
}

// MARK: - UITableViewDelegate extension
extension ConversationsViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let conversation = conversations[indexPath.row]
        openAnExisting(conversation: conversation)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80 //Since the userImageView is 100 by 100 and also adding the top and bottom 20 margins.
    }

    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        return .delete
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            let confirmDeletionAlert = UIAlertController(title: "Delete Chat?", message: "Are you sure you want to delete this chat?", preferredStyle: .alert)
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
            let deleteAction = UIAlertAction(title: "Delete", style: .default) { [weak self] action in
                guard let self = self else { return }
                let convoId = self.conversations[indexPath.row].id
                tableView.beginUpdates()
                DatabaseManager.shared.deleteAConversation(with: convoId) { conversationDeletedSuccessfully in
                    if conversationDeletedSuccessfully {
    //                    self?.conversations.remove(at: indexPath.row) - We do not have to remove the conversation in the conversations array as we are already observing the user's conversations in the method startListeningForUserConversations() which will automatically updated the conversations array with the newly updated conversation without the deleted conversation.
                        tableView.deleteRows(at: [indexPath], with: .left)
                        tableView.endUpdates()
                    }
                }
            }
            confirmDeletionAlert.addAction(cancelAction)
            confirmDeletionAlert.addAction(deleteAction)
            present(confirmDeletionAlert, animated: true, completion: nil)
        }
    }
    
}
