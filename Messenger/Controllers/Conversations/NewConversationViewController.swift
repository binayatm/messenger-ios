//
//  NewConversationViewController.swift
//  Messenger
//
//  Created by Binaya on 20/06/2022.
//

import UIKit
import JGProgressHUD
import Firebase

/// A view controller that handles the user's search processes to converse with other users.
final class NewConversationViewController: UIViewController {

    // MARK: - Properties
    private let searchBar: UISearchBar = {
        let searchBar = UISearchBar()
        searchBar.placeholder = "Search for users to chat..."
        return searchBar
    }()
    private let tableView: UITableView = {
        let tableView = UITableView()
        tableView.isHidden = true
        tableView.register(NewConversationTableViewCell.self,
                           forCellReuseIdentifier: NewConversationTableViewCell.identifier)
        return tableView
    }()
    private let noUsersResultsLabel: UILabel = {
        let label = UILabel()
        label.isHidden = true
        label.text = "No Users Found!"
        label.textAlignment = .center
        label.textColor = .darkGray
        label.font = .systemFont(ofSize: 21, weight: .medium)
        return label
    }()
    private let loadingSpinner = JGProgressHUD(style: .dark)
    private var users = [SearchResultUser]() {
        didSet {
            filteredUsers = users
            updateUI()
        }
    }
    private var filteredUsers = [SearchResultUser]()
    //Using a bool value instead of checking if the users array is empty.
    //Useful in the cases where our app only has 1 user and when that user
    //is the one searching for other users, the array will be empty.
    private var hasFetchedAllOfTheUsers = false
    var selectedAUserToChat: ((SearchResultUser) -> Void)?
        
    // MARK: - Lifecycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        setupNavBar()
        setupSubViews()
    }

    // MARK: - Methods
    private func setup() {
        view.backgroundColor = .systemBackground
        setupSearchBar()
        setupTableView()
        view.addSubview(noUsersResultsLabel)
        fetchAllUsers()
    }
    
    //Fetching all of the users once and storing them in an array instead of
    //searching for them every time a search happens.
    private func fetchAllUsers() {
        loadingSpinner.show(in: view)
        DatabaseManager.shared.fetchAllUsers { [weak self] result in
            guard let self = self else {
                print("Self is nil in NewConversationViewController searchForUsers method.")
                return
            }
            self.loadingSpinner.dismiss(animated: true)
            switch result {
            case .success(let users):
                self.filterCurrentUserOut(of: users)
                self.hasFetchedAllOfTheUsers = true
            case .failure(let error):
                print("Firebase search users error: \(error)")
                self.showErrorAlert(with: "No users found.", and: error.localizedDescription)
            }
        }
    }
    
    private func setupNavBar() {
        navigationController?.navigationBar.topItem?.titleView = searchBar
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(systemName: "xmark"), style: .done, target: self, action: #selector(didTapDoneBarButton))
    }
    
    @objc private func didTapDoneBarButton() {
        dismiss(animated: true)
    }
    
    private func setupTableView() {
        tableView.dataSource = self
        tableView.delegate = self
        view.addSubview(tableView)
    }
    
    private func setupSearchBar() {
        searchBar.delegate = self
    }
    
    private func setupSubViews() {
        tableView.frame = view.bounds //Hidden by default. Only shown when filterResults are available.
        noUsersResultsLabel.frame = CGRect(x: view.width / 4,
                                           y: (view.height - 200) / 2,
                                           width: view.width / 2,
                                           height: 200)
    }
    
    private func filterCurrentUserOut(of users: [[String: String]]) {
        guard let loggedInUserEmail = Auth.auth().currentUser?.email else {
            print("Error in filterCurrentUserOut: Firebase logged in user's email is nil!")
            return
        }
        let loggedInUserSafeEmail = DatabaseManager.getSafeEmail(with: loggedInUserEmail)
        var mutableUsers = users
        for (index, user) in users.enumerated() {
            if user["email"] == loggedInUserSafeEmail {
                mutableUsers.remove(at: index)
                break
            }
        }
        self.users = mutableUsers.compactMap({
            guard let name = $0["name"], let email = $0["email"] else { return nil }
            return SearchResultUser(name: name, email: email)
        })
    }
    
    private func updateUI() {
        if filteredUsers.isEmpty {
            noUsersResultsLabel.isHidden = false
            tableView.isHidden = true
        } else {
            noUsersResultsLabel.isHidden = true
            tableView.isHidden = false
            tableView.reloadData()
        }
    }

}

// MARK: - UITableV
extension NewConversationViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filteredUsers.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: NewConversationTableViewCell.identifier, for: indexPath) as! NewConversationTableViewCell
        cell.otherUser = filteredUsers[indexPath.row]
        return cell
    }
    
}

extension NewConversationViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedUser = filteredUsers[indexPath.row]
        tableView.deselectRow(at: indexPath, animated: true)
        dismiss(animated: true) { [weak self] in
            self?.selectedAUserToChat?(selectedUser)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }

}


// MARK: - UISearchBarDelegate extension
extension NewConversationViewController: UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        filterUsers(with: searchText)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        guard let text = searchBar.text, !text.replacingOccurrences(of: " ", with: "").isEmpty else {
            print("Search bar is nil or search bar text is empty.")
            return
        }
        searchBar.resignFirstResponder() //Remove the keyboard.
        filteredUsers.removeAll()
        loadingSpinner.show(in: view)
        filterUsers(with: text)
    }
    
    private func filterUsers(with query: String) {
        guard hasFetchedAllOfTheUsers else {
            print("Attempting to filter users when the users have not been fetched yet!")
            return
        }
        let filteredUsersFromSearchQuery: [SearchResultUser] = users.filter {
            return $0.name.lowercased().hasPrefix(query.lowercased())
        }
        filteredUsers = filteredUsersFromSearchQuery
        updateUI()
    }
    
}
