//
//  RegisterViewController.swift
//  Messenger
//
//  Created by Binaya on 20/06/2022.
//

import UIKit
import FirebaseAuth
import FirebaseDatabase
import JGProgressHUD

/// A View Controller that handles the registration processes.
final class RegisterViewController: UIViewController {

    // MARK: - Properties
    private let scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.clipsToBounds = true
        return scrollView
    }()
    
    private let profilePicImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(systemName: "person.crop.circle.fill")
        imageView.contentMode = .scaleAspectFit
        imageView.tintColor = UIColor(red: 0.27, green: 0.37, blue: 0.61, alpha: 1.00)
        imageView.layer.masksToBounds = true //Important to make it circular by clipping the overflowing image.
        imageView.layer.borderWidth = 1
        imageView.layer.borderColor = UIColor.lightGray.cgColor
        return imageView
    }()
    
    private let firstNameTextField: UITextField = {
        let textField = UITextField()
        textField.autocapitalizationType = .words
        textField.autocorrectionType = .no
        textField.returnKeyType = .continue
        textField.layer.cornerRadius = 12
        textField.layer.borderWidth = 1
        textField.layer.borderColor = UIColor.lightGray.cgColor
        textField.attributedPlaceholder = NSAttributedString(
            string: "First Name",
            attributes: [NSAttributedString.Key.foregroundColor: UIColor.placeholderText]
        )
        textField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 15, height: 0))
        textField.leftViewMode = .always
        textField.backgroundColor = .secondarySystemBackground
        return textField
    }()
    
    private let lastNameTextField: UITextField = {
        let textField = UITextField()
        textField.autocapitalizationType = .words
        textField.autocorrectionType = .no
        textField.returnKeyType = .continue
        textField.layer.cornerRadius = 12
        textField.layer.borderWidth = 1
        textField.layer.borderColor = UIColor.lightGray.cgColor
        textField.attributedPlaceholder = NSAttributedString(
            string: "Last Name",
            attributes: [NSAttributedString.Key.foregroundColor: UIColor.placeholderText]
        )
        textField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 15, height: 0))
        textField.leftViewMode = .always
        textField.backgroundColor = .secondarySystemBackground
        return textField
    }()
    
    private let emailTextField: UITextField = {
        let textField = UITextField()
        textField.autocapitalizationType = .none
        textField.autocorrectionType = .no
        textField.returnKeyType = .continue
        textField.layer.cornerRadius = 12
        textField.layer.borderWidth = 1
        textField.layer.borderColor = UIColor.lightGray.cgColor
        textField.attributedPlaceholder = NSAttributedString(
            string: "Email",
            attributes: [NSAttributedString.Key.foregroundColor: UIColor.placeholderText]
        )
        textField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 15, height: 0))
        textField.leftViewMode = .always
        textField.backgroundColor = .secondarySystemBackground
        return textField
    }()
    
    private let passwordTextField: UITextField = {
        let textField = UITextField()
        textField.autocapitalizationType = .none
        textField.isSecureTextEntry = true
        textField.autocorrectionType = .no
        textField.returnKeyType = .done
        textField.layer.cornerRadius = 12
        textField.layer.borderWidth = 1
        textField.layer.borderColor = UIColor.lightGray.cgColor
        textField.attributedPlaceholder = NSAttributedString(
            string: "Password",
            attributes: [NSAttributedString.Key.foregroundColor: UIColor.placeholderText]
        )
        textField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 15, height: 0))
        textField.leftViewMode = .always
        textField.backgroundColor = .secondarySystemBackground
        return textField
    }()
    
    private let registerButton: UIButton = {
        let button = UIButton()
        button.setTitle("Register", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.backgroundColor = UIColor(red: 0.27, green: 0.37, blue: 0.61, alpha: 1.00)
        button.layer.cornerRadius = 12
        button.layer.masksToBounds = true
        button.titleLabel?.font = .systemFont(ofSize: 20, weight: .bold)
        return button
    }()
    
    private let loadingSpinner = JGProgressHUD(style: .dark)
    
    // MARK: - @IBOutlets
    
    // MARK: - Lifecycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupNavBar()
    }
            
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        setupSubviewsLayout()
    }

    // MARK: - Methods
    private func setup() {
        setupView()
        setupSubviews()
        setupTextFields()
        setupProfilePicImageView()
        setupRegisterButton()
    }
    
    private func setupView() {
        view.backgroundColor = .systemBackground
        title = "Register"
    }
    
    private func setupSubviews() {
        view.addSubview(scrollView)
        scrollView.addSubview(profilePicImageView)
        scrollView.addSubview(firstNameTextField)
        scrollView.addSubview(lastNameTextField)
        scrollView.addSubview(emailTextField)
        scrollView.addSubview(passwordTextField)
        scrollView.addSubview(registerButton)
    }
    
    private func setupSubviewsLayout() {
        scrollView.frame = view.bounds
        let size = scrollView.width / 4
        profilePicImageView.frame = CGRect(x: (scrollView.width - size) / 2, y: 20, width: size, height: size)
        profilePicImageView.layer.cornerRadius = profilePicImageView.width / 2
        firstNameTextField.frame = CGRect(x: 30, y: profilePicImageView.bottom + 25, width: scrollView.width - 60, height: 52)
        lastNameTextField.frame = CGRect(x: 30, y: firstNameTextField.bottom + 15, width: scrollView.width - 60, height: 52)
        emailTextField.frame = CGRect(x: 30, y: lastNameTextField.bottom + 15, width: scrollView.width - 60, height: 52)
        passwordTextField.frame = CGRect(x: 30, y: emailTextField.bottom + 15 , width: scrollView.width - 60, height: 52)
        registerButton.frame = CGRect(x: 30, y: passwordTextField.bottom + 20, width: scrollView.width - 60, height: 50)
    }
    
    private func setupNavBar() {
        navigationController?.navigationBar.titleTextAttributes = [
            NSAttributedString.Key.foregroundColor: UIColor.systemBlue
        ]
        
        navigationController?.navigationBar.barTintColor = .systemBackground
        navigationController?.navigationBar.backgroundColor = .systemBackground
        navigationController?.navigationBar.tintColor = .systemBlue
    }
    
    private func setupTextFields() {
        [firstNameTextField, lastNameTextField, emailTextField, passwordTextField].forEach { textField in
            textField.delegate = self
            textField.textColor = .label
        }
    }
    
    private func setupProfilePicImageView() {
        let gesture = UITapGestureRecognizer(target: self, action: #selector(didTapProfilePic))
        gesture.numberOfTapsRequired = 1
        gesture.numberOfTouchesRequired = 1
        profilePicImageView.addGestureRecognizer(gesture)
        profilePicImageView.isUserInteractionEnabled = true
    }
    
    @objc private func didTapProfilePic() {
        presentPhotoSelectionOptionsActionSheet()
    }
    
    private func setupRegisterButton() {
        registerButton.addTarget(self, action: #selector(registerButtonTapped), for: .touchUpInside)
    }
    
    @objc private func registerButtonTapped() {
        loadingSpinner.show(in: view)
        resignTextFieldFirstResponder()
        guard let firstName = firstNameTextField.text, let lastName = lastNameTextField.text, let email = emailTextField.text, let password = passwordTextField.text, !firstName.isEmpty, !lastName.isEmpty, !email.isEmpty, !password.isEmpty, email.isValidEmail, password.count >= 6 else {
            loadingSpinner.dismiss(animated: true)
            showErrorAlert(with: "Error", and: "Please fill the textfields with valid information to register .")
            return
        }
        print("Firebase Registration!")
        //NOTE: The email is being used as the child for the database insertion. So we can check if an user exists through the email.
        DatabaseManager.shared.userEmailAlreadyExist(with: email) { [weak self] userEmailAlreadyExists in
            guard let strongSelf = self else {
                print("Self is nil!")
                return
            }
            guard !userEmailAlreadyExists else {
                DispatchQueue.main.async {
                    strongSelf.loadingSpinner.dismiss(animated: true)
                }
                strongSelf.showErrorAlert(with: "Registration Error", and: "Email provided already exists!")
                return
            }
            strongSelf.createANewUser(with: firstName, lastName: lastName, email: email, and: password)
        }
    }
    
    // MARK: - Firebase methods
    private func createANewUser(with firstName: String, lastName: String, email: String, and password: String) {
        FirebaseAuth.Auth.auth().createUser(withEmail: email, password: password) { [weak self] authDataResult, error in
            guard let strongSelf = self else {
                print("Self is nil!")
                return
            }
            guard let authDataResult = authDataResult, error == nil else {
                strongSelf.showErrorAlert(with: "Registration Error", and: "Error registering the user: \(error!)")
                return
            }
            print("Registration success with the new user: \(authDataResult.user)")
            UserDefaults.standard.set(email, forKey: GlobalConstants.UserDefaultKeys.emailKey)
            let newUser = ChatAppUser(firstName: firstName, lastName: lastName, email: email)
            DatabaseManager.shared.createUser(with: newUser) { userCreated in
                if userCreated {
                    UserDefaults.standard.set("\(firstName) \(lastName)", forKey: GlobalConstants.UserDefaultKeys.nameKey)
                    guard let image = strongSelf.profilePicImageView.image, let imageData = image.pngData() else {
                        strongSelf.showErrorAlert(with: "Error!", and: "Error getting the png data from the image to upload it to Firebase Storage.")
                        return
                    }
                    strongSelf.uploadUserProfilePicture(with: imageData, fileName: newUser.profilePictureFileName)
                } else {
                    strongSelf.showErrorAlert(with: "Error!", and: "Error creating a user in the real time database.")
                }
            }
        }
    }
    
    private func uploadUserProfilePicture(with imageData: Data, fileName: String) {
        DispatchQueue.main.async {
            StorageManager.shared.uploadProfilePicture(with: imageData, fileName: fileName) { [weak self] result in
                switch result {
                case .success(let imageDownloadUrlString):
                    self?.loadingSpinner.dismiss(animated: true)
                    print("Uploaded image download url: \(imageDownloadUrlString)")
                    UserDefaults.standard.set(imageDownloadUrlString, forKey: GlobalConstants.UserDefaultKeys.profilePictureDownloadUrlStringKey)
                    NotificationCenter.default.post(name: .didCompleteLoggingIn, object: nil)
                    self?.navigationController?.dismiss(animated: true, completion: nil) //Firebase automatically logs the user in upon a successful registration.
                case .failure(let uploadError):
                    self?.showErrorAlert(with: "Error!", and: "Error uploading to the profile picture to Firebase Storeage: \(uploadError)")
                }
            }
        }
    }
    
    private func resignTextFieldFirstResponder() {
        firstNameTextField.resignFirstResponder()
        lastNameTextField.resignFirstResponder()
        emailTextField.resignFirstResponder()
        passwordTextField.resignFirstResponder()
    }
    
    private func presentPhotoSelectionOptionsActionSheet() {
        let photoSelectionOptionActionSheetController = UIAlertController(title: "Upload profile picture", message: "Choose the preferred method to upload a profile picture.", preferredStyle: .actionSheet)
        photoSelectionOptionActionSheetController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        photoSelectionOptionActionSheetController.addAction(UIAlertAction(title: "Take a photo ", style: .default, handler: { [weak self] _ in
            self?.presentCamera()
        }))
        photoSelectionOptionActionSheetController.addAction(UIAlertAction(title: "Choose a photo", style: .default, handler: { [weak self] _ in
            self?.presentPhotosLibrary()
        }))
        present(photoSelectionOptionActionSheetController, animated: true, completion: nil)
    }
    
    private func presentCamera() {
        let imagePickerController = UIImagePickerController()
        imagePickerController.delegate = self
        imagePickerController.sourceType = .camera
        imagePickerController.allowsEditing = true
        present(imagePickerController, animated: true, completion: nil)
    }
    
    private func presentPhotosLibrary() {
        let imagePickerController = UIImagePickerController()
        imagePickerController.delegate = self
        imagePickerController.sourceType = .photoLibrary
        imagePickerController.allowsEditing = true
        present(imagePickerController, animated: true, completion: nil)
    }
    
}

// MARK: - UITextFieldDelegate extension
extension RegisterViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == firstNameTextField {
            lastNameTextField.becomeFirstResponder()
        } else if textField == lastNameTextField {
            emailTextField.becomeFirstResponder()
        } else if textField == emailTextField {
            passwordTextField.becomeFirstResponder()
        } else if textField == passwordTextField {
            registerButtonTapped()
        }
         return true
    }
    
}

// MARK: - UIImagePickerControllerDelegate extension
extension RegisterViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true, completion: nil)
        if let selectedCroppedImage = info[UIImagePickerController.InfoKey.editedImage] as? UIImage {
            profilePicImageView.image = selectedCroppedImage
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
}
