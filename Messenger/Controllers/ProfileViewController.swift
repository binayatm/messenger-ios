//
//  ProfileViewController.swift
//  Messenger
//
//  Created by Binaya on 20/06/2022.
//

import UIKit
import FirebaseAuth
import FBSDKLoginKit
import GoogleSignIn
import SDWebImage

/// A view controller that handles the user's profile processes.
final class ProfileViewController: UIViewController {

    // MARK: - Properties
    var viewModels = [ProfileViewModel]()
    var viewHasAlreadyLoaded = false
    var imagePath = ""
    private var loginObserver: NSObjectProtocol?
    
    // MARK: - @IBOutlets
    @IBOutlet weak var tableView: UITableView!
    
    // MARK: - Lifecycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        viewHasAlreadyLoaded = true
        setup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //Since we are presenting the login page when the user logs out, the profile vc is never destroyed and will be in the background.
        if viewHasAlreadyLoaded && imagePath.isEmpty {
            viewModels = []
            setupViewModels()
            reloadTableViewData()
        }
    }
    
    deinit {
        if let loginObserver = loginObserver {
            NotificationCenter.default.removeObserver(loginObserver)
        }
    }

    // MARK: - Methods
    private func setup() {
        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(ProfileTableViewCell.self, forCellReuseIdentifier: ProfileTableViewCell.identifier)
        createTableViewHeader()
        setupViewModels()
        setupLoginObserver()
    }
    
    private func setupLoginObserver() {
        loginObserver = NotificationCenter.default.addObserver(forName: .didCompleteLoggingIn, object: nil, queue: .main, using: { [weak self] _ in
            guard let self = self else { return }
            self.viewModels = []
            self.setupViewModels()
            self.reloadTableViewData()
        })
    }
    
    private func reloadTableViewData() {
        createTableViewHeader()
        tableView.reloadData()
    }
    
    private func setupViewModels() {
        let loggedInUserame = UserDefaults.standard.value(forKey: GlobalConstants.UserDefaultKeys.nameKey) as? String ?? "No Name"
        let loggedInUserEmail = UserDefaults.standard.value(forKey: GlobalConstants.UserDefaultKeys.emailKey) as? String ?? "No Email"
        viewModels.append(ProfileViewModel(profileSections: .info,
                                              title: "Name: \(loggedInUserame)",
                                              handler: nil))
        viewModels.append(ProfileViewModel(profileSections: .info,
                                              title: "Email: \(loggedInUserEmail)",
                                              handler: nil))
        viewModels.append(ProfileViewModel(profileSections: .logout,
                                           title: "Log Out",
                                           handler: { [weak self] in
            guard let self = self else { return }
            self.alertWithOKActionCancel(message: "Are you sure you want to log out?",
                                         title: "Log Out",
                                         style: .actionSheet,
                                         okTitle: "Logout",
                                         cancelTitle: "Cancel") { [weak self] in
                guard let strongSelf = self else {
                    return
                }
                //Facebook logout
                FBSDKLoginKit.LoginManager().logOut()
                //Google sign-out
                GIDSignIn.sharedInstance().signOut()
                //Firebase logout
                do {
                    // Send to login page.
                    try FirebaseAuth.Auth.auth().signOut()
                    DatabaseManager.shared.removeAllObservers()
                    let loginViewController = LoginViewController()
                    let navigationController = UINavigationController(rootViewController: loginViewController)
                    navigationController.modalPresentationStyle = .fullScreen
                    strongSelf.imagePath.removeAll()
                    UserDefaults.standard.set("", forKey: GlobalConstants.UserDefaultKeys.emailKey)
                    UserDefaults.standard.set("", forKey: GlobalConstants.UserDefaultKeys.nameKey)
                    UserDefaults.standard.set("", forKey: GlobalConstants.UserDefaultKeys.profilePictureDownloadUrlStringKey)
                    strongSelf.present(navigationController, animated: true) { [weak self] in
                        //Reset TabBar to the first tab so that when the user logs in again, he/she will be directed to the first tab instead of the profile tab.
                        self?.resetRootTabBarControllerTab()
                    }
                } catch {
                    strongSelf.showErrorAlert(with: "Firebase Error!", and: "Error signing out the user.")
                }
            }
        }))
    }
    
    private func createTableViewHeader() {
        guard let email = UserDefaults.standard.value(forKey: GlobalConstants.UserDefaultKeys.emailKey) as? String else {
            print("User email is nil for email key in UserDefaults.")
            return
        }
        let safeEmail = DatabaseManager.getSafeEmail(with: email)
        let imageFileName = safeEmail + "_profile_picture.png"
        let imagePath = "images/" + imageFileName
        self.imagePath = imagePath
        let headerView = UIView(frame: CGRect(x: 0,
                                        y: 0,
                                        width: self.view.width,
                                        height: 170))
        let imageView = getImageViewForProfilePic()
        headerView.addSubview(imageView)
        StorageManager.shared.getProfilePictureDownloadUrl(for: imagePath) { result in
            switch result {
            case.success(let url):
                imageView.sd_setImage(with: url, completed: nil)
            case.failure(let error):
                print("Failed to get the download url for the user's profile picture: \(error)")
            }
        }
        tableView.tableHeaderView = headerView
    }
    
    private func getImageViewForProfilePic() -> UIImageView {
        let imageView = UIImageView(frame: CGRect(x: (view.width - 125) / 2,
                                                  y: 25,
                                                  width: 120,
                                                  height: 120))
        imageView.contentMode = .scaleAspectFill
        imageView.backgroundColor = .white
        imageView.layer.cornerRadius = imageView.width / 2
        imageView.layer.borderWidth = 3
        imageView.layer.borderColor = UIColor.white.cgColor
        imageView.layer.masksToBounds = true
        return imageView
    }

    private func resetRootTabBarControllerTab() {
        if let tabBarViewController = UIApplication.shared.windows.first!.rootViewController as? UITabBarController {
            tabBarViewController.selectedIndex = 0
        }
    }

}

// MARK: - UITableViewDataSource extension
extension ProfileViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModels.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ProfileTableViewCell.identifier, for: indexPath) as! ProfileTableViewCell
        cell.setup(with: viewModels[indexPath.row])
        return cell
    }
    
}

// MARK: - UITableViewDelegate extension
extension ProfileViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        viewModels[indexPath.row] .handler?()
    }
    
}
