//
//  LocationPickerViewController.swift
//  Messenger
//
//  Created by Binaya on 17/02/2023.
//

import UIKit
import CoreLocation
import MapKit

/// A view controller that handles the user's location related processes.
final class LocationPickerViewController: UIViewController {
    
    // MARK: - Properties
    private var mapView: MKMapView = {
        let mapView = MKMapView()
        return mapView
    }()
    
    private var userPickedLocationCoordinates: CLLocationCoordinate2D?
    public var locationPicked: ((CLLocationCoordinate2D) -> Void)?
    private var isLocationPickable: Bool = true
    var locationManager = CLLocationManager()
    
    // MARK: - Initializers
    init(locationCoordinates: CLLocationCoordinate2D?) {
        userPickedLocationCoordinates = locationCoordinates
        isLocationPickable = locationCoordinates != nil ? false : true
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Lifecycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        setupSubViewsLayout()
        navigationItem.largeTitleDisplayMode = .never
    }
    
    // MARK: - Other methods
    private func setup() {
        view.backgroundColor = .systemBackground
        view.addSubview(mapView)
        locationManager.delegate = self
        if isLocationPickable {
            setupMapTapGesture()
            setupLocationManager()
        } else {
            //Just show the location of the coordinates that was passed.
            guard let userPickedLocationCoordinates = userPickedLocationCoordinates else { return }
            let pin = MKPointAnnotation()
            pin.coordinate = userPickedLocationCoordinates
            mapView.addAnnotation(pin)
            mapView.zoomToLocation(with: userPickedLocationCoordinates)
        }
    }
    
    private func setupSubViewsLayout() {
        mapView.frame = view.bounds
        if isLocationPickable {
            setupNavBar()
        }
    }
    
    private func setupLocationManager() {
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
    }
    
    private func setupNavBar() {
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Send",
                                                            style: .done,
                                                            target: self,
                                                            action: #selector(sendButtonTapped))
    }
    
    private func setupMapTapGesture() {
        let tapGesture = UITapGestureRecognizer(target: self,
                                                action: #selector(didTapMap(_:)))
        tapGesture.numberOfTapsRequired = 1
        tapGesture.numberOfTouchesRequired = 1
        mapView.isUserInteractionEnabled = true
        mapView.addGestureRecognizer(tapGesture)
    }
    
    // MARK: - @objc methods
    @objc private func sendButtonTapped() {
        guard let userPickedLocationCoordinates = userPickedLocationCoordinates else {
            showErrorAlert(with: "No Location", and: "Please select a location to send.")
            return }
        navigationController?.popViewController(animated: true)
        locationPicked?(userPickedLocationCoordinates)
    }
    
    @objc private func didTapMap(_ gesture: UITapGestureRecognizer) {
        let tappedLocationInMapView = gesture.location(in: mapView)
        let userPickedLocationCoordinates = mapView.convert(tappedLocationInMapView, toCoordinateFrom: mapView)
        self.userPickedLocationCoordinates = userPickedLocationCoordinates
        //Drop a pin on that location to show the user's tapped location.
        let pin = MKPointAnnotation()
        pin.coordinate = userPickedLocationCoordinates
        mapView.removeAnnotations(mapView.annotations) 
        mapView.addAnnotation(pin)
    }
    
}

// MARK: - CLLocationManagerDelegate extension
extension LocationPickerViewController: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        locationManager.stopUpdatingLocation()
        if let coordinate = locations.last?.coordinate {
            mapView.zoomToLocation(with: coordinate, coordinateDelta: 0.002)
        }
    }
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(error.localizedDescription)
    }
    
}
