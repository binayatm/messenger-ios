//
//  Conversation.swift
//  Messenger
//
//  Created by Binaya on 05/10/2022.
//

import Foundation

struct Conversation {
    
    let id: String
    let otherUserName: String
    let otherUserEmail: String
    let latestMessage: LatestMessage
    let creationDate: String
    
}

struct LatestMessage {
    
    let date: String
    let text: String
    let isRead: Bool
    
}
