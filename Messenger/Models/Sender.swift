//
//  Sender.swift
//  Messenger
//
//  Created by Binaya on 11/07/2022.
//

import Foundation
import MessageKit

struct Sender: SenderType {
    
    var photoURL: String
    var senderId: String
    var displayName: String
    
}
