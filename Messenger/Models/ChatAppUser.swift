//
//  ChatAppUser.swift
//  Messenger
//
//  Created by Binaya on 28/06/2022.
//

import Foundation

struct ChatAppUser {
    
    // MARK: - Properties
    let firstName: String
    let lastName: String
    let email: String
    
    // MARK: - Computed properties
    var safeEmail: String {
        var safeEmail = email.replacingOccurrences(of: ".", with: "-")
        safeEmail = safeEmail.replacingOccurrences(of: "@", with: "-")
        return safeEmail
    }
    var profilePictureFileName: String {
       return "\(safeEmail)_profile_picture.png"
    }
    
}
