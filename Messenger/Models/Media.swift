//
//  Media.swift
//  Messenger
//
//  Created by Binaya on 13/01/2023.
//

import Foundation
import MessageKit

struct Media: MediaItem {
    
    var url: URL?
    var image: UIImage?
    var placeholderImage: UIImage
    var size: CGSize
    
}
