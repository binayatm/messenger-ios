//
//  Message.swift
//  Messenger
//
//  Created by Binaya on 11/07/2022.
//

import Foundation
import MessageKit

struct Message: MessageType {
    
    var sender: SenderType
    var messageId: String
    var sentDate: Date
    var kind: MessageKind

}

//Created this extension to return the MessageKind values that are enums in string so that we can save the data in Firebase as an string.
extension MessageKind {
    
    var stringValue: String {
        switch self {
        case .text(_):
            return "text"
        case .attributedText(_):
            return "attributedText"
        case .photo(_):
            return "photo"
        case .video(_):
            return "video"
        case .location(_):
            return "location"
        case .emoji(_):
            return "emoji"
        case .audio(_):
            return "audio"
        case .contact(_):
            return "contact"
        case .custom(_):
            return "custom"
        }
    }
    
}
