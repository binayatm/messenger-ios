//
//  SearchResultUser.swift
//  Messenger
//
//  Created by Binaya on 31/01/2023.
//

import Foundation

struct SearchResultUser {
    
    let name: String
    let email: String
    
}
