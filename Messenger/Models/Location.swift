//
//  Location.swift
//  Messenger
//
//  Created by Binaya on 17/02/2023.
//

import Foundation
import MessageKit
import CoreLocation

struct Location: LocationItem {
    
    var location: CLLocation
    var size: CGSize
    
}
