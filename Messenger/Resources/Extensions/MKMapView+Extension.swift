//
//  MKMapView+Extension.swift
//  Messenger
//
//  Created by Binaya on 21/02/2023.
//

import UIKit
import MapKit

extension MKMapView {
    
    /// Zooms the MapView to the given coordinates by the zoom longitude and latitude delta values of 0.008
    func zoomToLocation(with coordinates: CLLocationCoordinate2D, coordinateDelta: CLLocationDegrees = 0.008) {
        let coordinateSpan = MKCoordinateSpan(latitudeDelta: coordinateDelta, longitudeDelta: coordinateDelta) //Change value to update zoom value.
        let coordinateRegion: MKCoordinateRegion = MKCoordinateRegion(center: coordinates,
                                                                      span: coordinateSpan)
        setRegion(coordinateRegion, animated: true)
        regionThatFits(coordinateRegion)
        isZoomEnabled = true
    }
    
}
