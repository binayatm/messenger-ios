//
//  Notification.Name+Extension.swift
//  Messenger
//
//  Created by Binaya on 07/07/2022.
//

import Foundation

extension Notification.Name {
    
    /// Google login notification name
    static let didCompleteGoogleSignIn = Notification.Name("didCompleteGoogleSignIn")
    
    /// Firebase login notification name
    static let didCompleteLoggingIn = Notification.Name("didCompleteLoggingIn")
    
    /// User's first conversation creation notification name
    static let firstConversationCreated = Notification.Name("firstConversationCreated")
    
}
