//
//  UIViewController+Extension.swift
//  Messenger
//
//  Created by Binaya on 21/06/2022.
//

import UIKit

extension UIViewController {

    /// Returns an UIAlertController with the title, message and style.
    func getAlert(message: String?, title: String?, style: UIAlertController.Style? = .alert) -> UIAlertController {
        return UIAlertController(title: title, message: message, preferredStyle: style ?? .alert)
    }
    
    /// Displays an error alert with a title and a message.
    public func showErrorAlert(with title: String = "Error", and message: String = "Something went wrong!") {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let closeAction = UIAlertAction(title: "Close", style: .cancel, handler: nil)
        alertController.addAction(closeAction)
        present(alertController, animated: true, completion: nil)
    }
    
    /// Displays an alert with an OK and Cancel actions.
    func alertWithOKActionCancel(message: String = "Error Occured", title: String? = "Alert",  style: UIAlertController.Style? = .alert, okTitle: String = "OK" ,cancelTitle: String = "Cancel",okAction: (()->())? = nil) {
        let alertController = getAlert(message: message, title: title, style: style)
        alertController.addAction(title: okTitle, style: .default, handler: okAction)
        alertController.addAction(title: cancelTitle, style: .cancel, handler: nil)
        present(alertController, animated: true, completion: nil)
    }
    
}
