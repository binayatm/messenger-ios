//
//  AppDelegate.swift
//  Messenger
//
//  Created by Binaya on 20/06/2022.
//

// So it turned out that I have to import FBSDKCoreKit instead of FacebookCore which was not mentioned in facebook's documentation. And it should be imported strictly using the .xcworkspace file which was also not mentioned in the documentation. The documentation only mentions to import FacebookCore.
import FBSDKCoreKit
import UIKit
import FirebaseCore
import GoogleSignIn
import FirebaseAuth
import IQKeyboardManagerSwift
import AVKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        FirebaseApp.configure()
        ApplicationDelegate.shared.application(
            application,
            didFinishLaunchingWithOptions: launchOptions
        )
        GIDSignIn.sharedInstance().clientID = FirebaseApp.app()?.options.clientID
        GIDSignIn.sharedInstance().delegate = self
        setupKeyboard()
        let audioSession = AVAudioSession.sharedInstance()
        do {
          try audioSession.setCategory(.playback, mode: .moviePlayback)
        } catch {
          print("Failed to set audioSession category to playback in Picture in Picture Playback.")
        }
        return true
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        ApplicationDelegate.shared.application(
            app,
            open: url,
            sourceApplication: options[UIApplication.OpenURLOptionsKey.sourceApplication] as? String,
            annotation: options[UIApplication.OpenURLOptionsKey.annotation]
        )
        return GIDSignIn.sharedInstance().handle(url)
    }
    
    // MARK: - IQKeyboardManagerSwift
    private func setupKeyboard() {
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.enableAutoToolbar = false
        IQKeyboardManager.shared.shouldResignOnTouchOutside = true
        //To make sure message bubble is not on the navigation area.
        IQKeyboardManager.shared.disabledDistanceHandlingClasses.append(ChatViewController.self)
    }
    
    // MARK: - GoogleSignIn
    private func signGoogleUserInFirebase(with user: GIDGoogleUser) {
        guard let firstName = user.profile.givenName, let lastName = user.profile.familyName, let email = user.profile.email else {
            print("User's profile details are nil")
            return
        }
        DatabaseManager.shared.userEmailAlreadyExist(with: user.profile.email) { emailIsUnique in
            if !emailIsUnique {
                let firebaseUser = ChatAppUser(firstName: firstName, lastName: lastName, email: email)
                DatabaseManager.shared.createUser(with: firebaseUser) { [weak self] userCreated in
                    if userCreated {
                        //Check if google user profile picture exists. Note: Facebook does not have this feature.
                        if user.profile.hasImage {
                            guard let googleUserImageUrl = user.profile.imageURL(withDimension: 200) else {
                                print("Google user image url is nil!")
                                return
                            }
                            self?.downloadGoogleUserProfilePicture(with: googleUserImageUrl, and: firebaseUser.profilePictureFileName)
                        }
                    } else {
                        print("Error creating a new user using the Google logged in user in Firebase.")
                    }
                }
            } else {
                print("Email already exists in the database for this google user so not creating a new database user.")
            }
        } 
        guard let authentication = user.authentication else {
            print("Google user authenticaiton is nil!")
            return
        }
        signGoogleUserInFirebase(with: authentication, fullName: "\(firstName) \(lastName)", and: email)
    }
    
    private func signGoogleUserInFirebase(with authentication: GIDAuthentication, fullName: String, and email: String) {
        let credential = GoogleAuthProvider.credential(withIDToken: authentication.idToken, accessToken: authentication.accessToken)
        FirebaseAuth.Auth.auth().signIn(with: credential) { authDataResult, error in
            guard authDataResult != nil, error == nil else {
                if let error = error {
                    print("Error signing in using Google. Multi-factor authentication may be needed: \(error)")
                }
                return
            }
            print("Google Sign-In with Firebase success! ")
            UserDefaults.standard.set(email, forKey: GlobalConstants.UserDefaultKeys.emailKey)
            UserDefaults.standard.set(fullName, forKey: GlobalConstants.UserDefaultKeys.nameKey)
            NotificationCenter.default.post(name: .didCompleteGoogleSignIn, object: nil)
        }
    }
    
    private func downloadGoogleUserProfilePicture(with profilePicUrl: URL, and pictureFileName: String) {
        print("Downloading the data from Google user's profile picture url.")
        URLSession.shared.dataTask(with: profilePicUrl) { [weak self] data, _, error in
            guard let data = data, error == nil else {
                print("Error downloading the Google user's profile picture data.")
                return
            }
            self?.uploadUserProfilePicture(with: data, fileName: pictureFileName)
        }.resume()
    }
    
    private func uploadUserProfilePicture(with imageData: Data, fileName: String) {
        print("Uploading the downloaded bytes data from the Google user's profile picture url.")
        StorageManager.shared.uploadProfilePicture(with: imageData, fileName: fileName) { result in
            switch result {
            case .success(let imageDownloadUrlString):
                print("Uploaded image download url: \(imageDownloadUrlString)")
                UserDefaults.standard.set(imageDownloadUrlString, forKey: GlobalConstants.UserDefaultKeys.profilePictureDownloadUrlStringKey)
            case .failure(let uploadError):
                print("Error uploading to the profile picture to Firebase Storeage: \(uploadError)")
            }
        }
    }
    
}

// MARK: - GIDSignInDelegate extension
extension AppDelegate: GIDSignInDelegate {
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        guard error == nil else {
            print("Error signing in with google account: \(error!)")
            return
        }
        guard let user = user, let userProfile = user.profile else {
            print("User variable and it's profile property nil!")
            return
        }
        print("Successfully signed in with google user: \(userProfile)")
        signGoogleUserInFirebase(with: user)
    }
    
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!, withError error: Error!) {
        print("Google user was disconnected.")
    }
    
}

