//
//  GlobalConstants.swift
//  Messenger
//
//  Created by Binaya on 01/08/2022.
//

import Foundation

struct GlobalConstants {
    
    struct UserDefaultKeys {
        static let emailKey = "userEmail"
        static let nameKey = "name"
        static let profilePictureDownloadUrlStringKey = "profilePictureDownloadURL"
    }
    
}
