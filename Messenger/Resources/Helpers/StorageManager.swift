//
//  StorageManager.swift
//  Messenger
//
//  Created by Binaya on 11/07/2022.
//

import Foundation
import FirebaseStorage

/// Manager object to fetch & upload files to Firebase storage.
final class StorageManager {
    
    // MARK: - Singleton
    static let shared = StorageManager()
    
    // MARK: - Initalizers
    /// Private initializer that will force us to use the shared instance of this class instead of creating other instances of this class.
    private init() {}
    
    // MARK: - Properties
    private let storage = Storage.storage().reference() //Only this singleton class will have reference to the storage.
    
    // MARK: - FirebaseStorageErrors Enum
    public enum FirebaseStorageErrors: Error {
        
        case failedToUploadImage
        case failedToUploadVideo
        case failedToGetDownloadUrl
        
        public var localizedDescription: String {
            switch self {
            case .failedToUploadImage:
                return "Failed to upload the image to Firebase storage."
            case .failedToUploadVideo:
                return "Failed to upload the video URL to the Firebase storage."
            case .failedToGetDownloadUrl:
                return "Failed to get the download url for the specified path."
            }
        }
        
    }
    
    // MARK: - Type alias
    public typealias MediaContentUploadCompletion = (Result<String, Error>) -> Void
    
    // MARK: - Methods
    /// Uploads the picture to the Firebase storage and returns a completion with a URL string to download the image.
    /// Parameters
    /// - `with`: The image data to be uploaded.
    /// - `fileName`: The file name for the image data.
    /// - `completion`: Async closure to return with result.
    public func uploadProfilePicture(with imageData: Data,
                                     fileName: String,
                                     completion: @escaping MediaContentUploadCompletion) {
        storage.child("images/\(fileName)").putData(imageData, metadata: nil) { [weak self] storageMetaData, error in
            guard let strongSelf = self else {
                print("Self is nil in upload profile picture method of the StorageManager singleton class.")
                return
            }
            guard error == nil else {
                print("Error uploading the image data to firebase: \(error!)")
                completion(.failure(FirebaseStorageErrors.failedToUploadImage))
                return
            }
            strongSelf.storage.child("images/\(fileName)").downloadURL { url, error in
                guard let url = url, error == nil else {
                    print("Firebase image upload complete but failed to get the download URL: \(error!)")
                    completion(.failure(FirebaseStorageErrors.failedToGetDownloadUrl))
                    return
                }
                let imageDownloadUrlString = url.absoluteString
                print("Uploaded image's download url string: \(imageDownloadUrlString)")
                completion(.success(imageDownloadUrlString))
            }
        }
    }
    
    /// Uploads the image that will be sent in a conversation message.
    /// Parameters
    /// - `with`: The message image data to be uploaded.
    /// - `fileName`: The message image file name for the image data.
    /// - `completion`: Async closure to return with result.
    public func uploadMessagePhoto(with imageData: Data,
                                   fileName: String,
                                   completion: @escaping MediaContentUploadCompletion) {
        storage.child("message_images/\(fileName)").putData(imageData, metadata: nil) { [weak self] storageMetaData, error in
            guard let strongSelf = self, error == nil else {
                print("Error in uploadMessagePhoto: Self is nil and failed to upload the image data to firebase!")
                completion(.failure(FirebaseStorageErrors.failedToUploadImage))
                return
            }
            strongSelf.storage.child("message_images/\(fileName)").downloadURL { url, error in
                guard let url = url, error == nil else {
                    print("Firebase image upload complete but failed to get the download URL: \(error!)")
                    completion(.failure(FirebaseStorageErrors.failedToGetDownloadUrl))
                    return
                }
                let imageDownloadUrlString = url.absoluteString
                print("Uploaded image's download url string: \(imageDownloadUrlString)")
                completion(.success(imageDownloadUrlString))
            }
        }
    }
    
    /// Returns the download url for the provided image path for the Firebase storage.
    /// Parameters
    /// - `for`: The image path for the user's profile picture.
    /// - `completion`: Async closure to return with result.
    public func getProfilePictureDownloadUrl(for path: String,
                                             completion: @escaping (Result<URL, Error>) -> Void) {
        let reference = storage.child(path)
        reference.downloadURL { url, error in
            guard let url = url, error == nil else {
                completion(.failure(FirebaseStorageErrors.failedToGetDownloadUrl))
                return
            }
            completion(.success(url))
        }
    }
    
    /// Uploads the video that will be sent in a conversation message.
    /// Parameters
    /// - `with`: The video file's URL to be uploaded.
    /// - `fileName`: The video file URL's name.
    /// - `completion`: Async closure to return with result.
    public func uploadMessageVideo(with videoFileURL: URL,
                                   fileName: String,
                                   completion: @escaping MediaContentUploadCompletion) {
        storage.child("message_videos/\(fileName)").putFile(from: videoFileURL, metadata: nil) { [weak self] storageMetaData, error in
            guard let strongSelf = self, error == nil else {
                print("Error in uploadMessageVideo: Self is nil and failed to upload the video file url to firebase!")
                completion(.failure(FirebaseStorageErrors.failedToUploadVideo))
                return
            }
            strongSelf.storage.child("message_videos/\(fileName)").downloadURL { url, error in
                guard let url = url, error == nil else {
                    print("Firebase video file url upload complete but failed to get the download URL: \(error!)")
                    completion(.failure(FirebaseStorageErrors.failedToGetDownloadUrl))
                    return
                }
                let videoDownloadUrlString = url.absoluteString
                print("Uploaded video's download url string: \(videoDownloadUrlString)")
                completion(.success(videoDownloadUrlString))
            }
        }
    }
    
}
