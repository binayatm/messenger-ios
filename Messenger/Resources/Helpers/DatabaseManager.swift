//
//  DatabaseManager.swift
//  Messenger
//
//  Created by Binaya on 28/06/2022.
//

import Foundation
import FirebaseDatabase
import MessageKit
import CoreLocation

// Creating a final class because we do not want this singleton class to be subclassed.
/// Manager object to read and write data to the real-time Firebase database.
final class DatabaseManager {
    
    // MARK: - Singleton
    /// Shared instance of class
    static let shared = DatabaseManager()
    
    // MARK: - Initalizers
    // When we are reading and writing to the database, we do not want to cause various performance issues where one controller read some data in the middle of another controller writing some data.
    /// Private initializer that will force us to use the shared instance of this class instead of creating other instances of this class.
    private init() {}
    
    // MARK: - Properties
    private let database = Database.database().reference() //Only this singleton class will have reference to the database.
    private var observers = [DatabaseReference]()
    
    // MARK: - Methods
    /// Returns a safe email format for Firebase database.
    /// Parameters
    /// - `email`: Target email to be converted.
    static func getSafeEmail(with emailAddress: String) -> String {
        var safeEmail = emailAddress.replacingOccurrences(of: ".", with: "-")
        safeEmail = safeEmail.replacingOccurrences(of: "@", with: "-")
        return safeEmail
    }
    
    /// Removes all of the observers of the conversations and the messages database references.
    public func removeAllObservers() {
        observers.forEach {
            $0.removeAllObservers()
        }
        observers = []
    }
    
}

// MARK: - Contains a generic method to get data for a given path
extension DatabaseManager {
    
    /// Returns a dictionary node for a given child path.
    /// Parameters
    /// - `path`: Target child path to get data.
    /// - `completion`: Async closure to return with result.
    public func getData(for path: String, completion: @escaping(Result<Any, Error>) -> ()) {
        database.child(path).observeSingleEvent(of: .value) { snapshot in
            guard let value = snapshot.value else {
                completion(.failure(DatabaseError.failedToFetch))
                return
            }
            completion(.success(value))
        }
    }
    
}

// MARK: - User related extension
extension DatabaseManager {
    
    public enum DatabaseError: Error {
        
        case failedToFetch
        case emailIsNilInUserDefaults
        case conversationDoesNotExist
        
        public var localizedDescription: String {
            switch self {
            case .failedToFetch:
                return "Failed to fetch data."
            case .emailIsNilInUserDefaults:
                return "The user's email is nil in the User Defaults."
            case .conversationDoesNotExist:
                return "The conversation does not exist."
            }
        }
        
    }
    
    // MARK: - Methods
    // NOTE: NOSQL db works using JSON. The child of the database refers a key of the key-value pair.

    /// Ensures that the email is unique in the Firebase Realtime database.
    /// Returns false if user's email does not exist in the database and false if otherwise.
    /// Note: It has a completion handler as the database method is asynchronous.
    /// Parameters
    /// - `email`: Target email to be checked.
    /// - `completion`: Async closure to return with result.
    public func userEmailAlreadyExist(with email: String, completion: @escaping ((Bool) -> Void)) {
        let safeEmail = DatabaseManager.getSafeEmail(with: email)
         //Firebase database allows us to observe value changes on any entry in our NoSQL db by specifying the child that we want to observe for and the type of observation that we want. We only want to observe a single event for this method which is to query the database once.
        database.child(safeEmail).observeSingleEvent(of: .value) { snapshot in
            guard (snapshot.value as? [String: Any]) != nil else {
                completion(false)
                return
            }
            completion(true) //Returning true since the email already exists.
        }
    }
    
    /// Creates a new user in the Firebase Realtime database with the ChatAppUser object.
    /// Parameters
    /// - `user`: Target user to be created.
    /// - `completion`: Async closure to return with result.
    public func createUser(with user: ChatAppUser, completion: @escaping (Bool) -> Void) {
        //Email has to be unique.
        database.child(user.safeEmail).setValue([
            "first_name": user.firstName,
            "last_name": user.lastName
        ]) { [weak self] error, _ in
            guard error == nil else {
                completion(false)
                print("Failed to create a new user in the database: \(error!)")
                return
            }
            self?.database.child("users").observeSingleEvent(of: .value, with: { snapshot in
                //Checking to see if the users collection exists.
                if var usersCollection = snapshot.value as? [[String: String]] {
                    //Append to the existing users collection.
                    let newUser = [
                        "name": user.firstName + " " + user.lastName,
                        "email": user.safeEmail
                    ]
                    usersCollection.append(newUser)
                    //Overriding the previous collection that does not have the new user.
                    self?.database.child("users").setValue(usersCollection, withCompletionBlock: { error, _ in
                        guard error == nil else {
                            completion(false)
                            print("Failed to append a new user to the users database: \(error!)")
                            return
                        }
                        completion(true)
                    })
                } else {
                    //Create a new users collection
                    let newUsersCollection: [[String: String]] = [
                        [
                            "name": user.firstName + " " + user.lastName,
                            "email": user.safeEmail
                        ]
                    ]
                    self?.database.child("users").setValue(newUsersCollection, withCompletionBlock: { error, _ in
                        guard error == nil else {
                            completion(false)
                            print("Failed to create a new users collection in the database: \(error!)")
                            return
                        }
                        completion(true)
                    })
                }
            })
            completion(true)
        }
    }
    
    /// Fetches all of the users from Firebase.
    /// Parameters
    /// - `completion`: Async closure to return with result.
    public func fetchAllUsers(completion: @escaping (Result<[[String: String]], Error>) -> Void) {
        database.child("users").observeSingleEvent(of: .value) { snapshot in
            guard let users = snapshot.value as? [[String: String]] else {
                print("Snapshot value is nil!")
                completion(.failure(DatabaseError.failedToFetch))
                return
            }
            completion(.success(users))
        }
    }
    
}

// MARK: - Getting conversations & sending messages.
extension DatabaseManager {
    
    //The schema that we want in the database needs to scale in 2 ways:
    
    //1) We want to observe all of the users' conversation and update when
    //a new conversation is added to the database in real-time on the
    //ConversationsViewController's table view.
    
    //2) When a new message is added to the database in a given conversation,
    //we also want to update the ChatViewController's table view in real time.
    
    /// Creates a new conversation in the database with the user's email and the first text message.
    /// Parameters
    /// - `with`: The recipient's email address.
    /// - `recipientName`: The recipient's name.
    /// - `firstTextMessage`: The user's first text message object.
    /// - `completion`: Async closure to return with result.
    public func createANewConversation(with recipientEmail: String,
                                       recipientName: String,
                                       firstTextMessage: Message,
                                       completion: @escaping (Conversation?) -> Void) {
        guard let currentUserEmail = UserDefaults.standard.value(forKey: GlobalConstants.UserDefaultKeys.emailKey) as? String, let currentUserName = UserDefaults.standard.value(forKey: GlobalConstants.UserDefaultKeys.nameKey) as? String else {
            return
        }
        let currentUserSafeEmail = DatabaseManager.getSafeEmail(with: currentUserEmail)
        let userChildNodeReference = database.child(currentUserSafeEmail)
        userChildNodeReference.observeSingleEvent(of: .value) { [weak self] snapshot in
            guard var userNode = snapshot.value as? [String: Any] else { //userNode is a var as we will modify it.
                completion(nil)
                print("Error in createANewConversation method: User not found!")
                return
            }
            let sentDateString = ChatViewController.dateFormatter.string(from: firstTextMessage.sentDate)
            var latestMessage = ""
            switch firstTextMessage.kind {
            case .text(let text):
                latestMessage = text
            case .attributedText(_):
                break
            case .photo(let mediaItem):
                if let imageUrlString = mediaItem.url?.absoluteString {
                    latestMessage = imageUrlString
                }
            case .video(let mediaItem):
                if let videoUrlString = mediaItem.url?.absoluteString {
                    latestMessage = videoUrlString
                }
            case .location(let locationData):
                //The order matters here as we will be rendering the location with the longitude first, followed by the latitude.
                latestMessage = "\(locationData.location.coordinate.longitude),\(locationData.location.coordinate.latitude)"
                break
            case .emoji(_):
                break
            case .audio(_):
                break
            case .contact(_):
                break
            case .custom(_):
                break
            }
            //NOTE: Conversation dictionary's id will be the message's id prefixed with the "conversation_".
            let conversationId = "conversation_\(firstTextMessage.messageId)"
            let newConversationDataForCurrentUser: [String: Any] = [
                "id": conversationId,
                "other_user_email": recipientEmail,
                "other_user_name": recipientName,
                "latest_message": [
                    "date": sentDateString,
                    "message": latestMessage,
                    "is_read": false
                ],
                "created_at": ChatViewController.dateFormatter.string(from: Date())
            ]
            let newConversationDataForRecipient: [String: Any] = [
                "id": conversationId,
                "other_user_email": currentUserSafeEmail,
                "other_user_name": currentUserName,
                "latest_message": [
                    "date": sentDateString,
                    "message": latestMessage,
                    "is_read": false
                ],
                "created_at": ChatViewController.dateFormatter.string(from: Date())
            ]
            //FOR CURRENTLY LOGGED IN USER: Checking to see if the conversations collection exists for the current logged in user.
            if var conversations = userNode["conversations"] as? [[String: Any]] {
                //Append a new conversation dictionary to the conversations collection.
                conversations.append(newConversationDataForCurrentUser)
                //Override the previous conversations collection that does not have the new conversation.
                userNode["conversations"] = conversations
                //Overriding the previous user dictionary that does not have the new conversations collections.
                userChildNodeReference.setValue(userNode) { error, _ in
                    guard error == nil else {
                        completion(nil)
                        print("Error: Error creating a conversations collection for the user dictionary in the database!")
                        return
                    }
                    self?.createAConversationNode(with: recipientName,
                                                  conversationDictionary: newConversationDataForCurrentUser,
                                                  firstTextMessage: firstTextMessage,
                                                  completion: completion)
                }
            } else {
                //Creating a new conversations collection for the user if it does not exist.
                userNode["conversations"] = [
                    newConversationDataForCurrentUser
                ]
                //Overriding the previous user dictionary that does not have the conversations collections.
                userChildNodeReference.setValue(userNode) { error, _ in
                    guard error == nil else {
                        completion(nil)
                        print("Error: Error creating a conversations collection for the user dictionary in the database!")
                        return
                    }
                    self?.createAConversationNode(with: recipientName,
                                                  conversationDictionary: newConversationDataForCurrentUser,
                                                  firstTextMessage: firstTextMessage,
                                                  completion: completion)
                }
            }
            //FOR RECIPIENT USER:
            //NOTE: Here we are observing the conversations node ONCE directly unlike the case for the current user above where we are observing the userNode ONCE which is the root user noot and accessing the conversations using the dictionary notation "userNode["conversations"]".
            self?.database.child("\(recipientEmail)/conversations").observeSingleEvent(of: .value) { snapshot in
                if var conversations = snapshot.value as? [[String: Any]] {
                    conversations.append(newConversationDataForRecipient)
                    self?.database.child("\(recipientEmail)/conversations").setValue(conversations)
                } else {
                    self?.database.child("\(recipientEmail)/conversations").setValue([newConversationDataForRecipient])
                }
            }
        }
    }
    
    /// Creates a Conversation node in the Firebase with the ConversationId as the key.
    /// Parameters
    /// - `with`: The recipient's email address.
    /// - `conversationDictionary`: The first conversation between the sender and the recipient.
    /// - `firstTextMessage`: The user's first text message object.
    /// - `completion`: Async closure to return with result.
    private func createAConversationNode(with recipientName: String,
                                         conversationDictionary: [String: Any],
                                         firstTextMessage: Message,
                                         completion: @escaping (Conversation?) -> Void) {
        guard let currentUserEmail = UserDefaults.standard.value(forKey: GlobalConstants.UserDefaultKeys.emailKey) as? String else {
            print("Error in createAConversationNode: Email key is nil in userdefaults.")
            completion(nil)
            return
        }
        let safeCurrentUserEmail = DatabaseManager.getSafeEmail(with: currentUserEmail)
        var message = ""
        switch firstTextMessage.kind {
        case .text(let text):
            message = text
        case .attributedText(_):
            break
        case .photo(let mediaItem):
            if let imageUrlString = mediaItem.url?.absoluteString {
                message = imageUrlString
            }
        case .video(let mediaItem):
            if let videoUrlString = mediaItem.url?.absoluteString {
                message = videoUrlString
            }
        case .location(let locationData):
            //The order matters here as we will be rendering the location with the longitude first, followed by the latitude.
            message = "\(locationData.location.coordinate.longitude),\(locationData.location.coordinate.latitude)"
            break
        case .emoji(_):
            break
        case .audio(_):
            break
        case .contact(_):
            break
        case .custom(_):
            break
        }
        let sentDateString = ChatViewController.dateFormatter.string(from: firstTextMessage.sentDate)
        let newMessageDictionaryData: [String: Any] = [
            "id": firstTextMessage.messageId,
            "receiver_name": recipientName,
            "type": firstTextMessage.kind.stringValue,
            "content": message,
            "date": sentDateString,
            "sender_email": safeCurrentUserEmail,
            "is_read": false
        ]
        let conversationRootNode: [String: Any] = [
            "messages": [
                newMessageDictionaryData
            ]
        ]
        if let conversationId = conversationDictionary["id"] as? String {
            database.child(conversationId).setValue(conversationRootNode) { error, _ in
                guard error == nil,
                      let recipientEmail = conversationDictionary["other_user_email"] as? String,
                      let recipientName = conversationDictionary["other_user_name"] as? String,
                      let latestMessage = conversationDictionary["latest_message"] as? [String: Any],
                      let convoCreationDate = conversationDictionary["created_at"] as? String,
                      let sentDate = latestMessage["date"] as? String,
                      let messageIsRead = latestMessage["is_read"] as? Bool,
                      let message = latestMessage["message"] as? String else {
                          print("Error in finishedCreatingConversation method: Failed to create a conversation root node.")
                          completion(nil)
                          return
                      }
                let latestMessageObject = LatestMessage(date: sentDate,
                                                        text: message,
                                                        isRead: messageIsRead)
                let conversation = Conversation(id: conversationId,
                                    otherUserName: recipientName,
                                    otherUserEmail: recipientEmail,
                                    latestMessage: latestMessageObject,
                                    creationDate: convoCreationDate)
                completion(conversation)
            }
        } else {
            completion(nil)
        }
    }
    
    /// Appends a new message to an existing conversation with the conversation's id.
    /// Parameters
    /// - `conversationId`: The conversation ID.
    /// - `newMessage`: The new message that is to be appended to the existing converation.
    /// - `recipientEmail`: The recipient's email.
    /// - `recipientName`: The recipient's name.
    /// - `completion`: Async closure to return with result.
    public func appendMessageTo(conversationId existingConversationId: String,
                              newMessage: Message,
                              recipientEmail: String,
                              recipientName: String,
                              completion: @escaping (Bool) -> ()) {
        guard let currentUserEmail = UserDefaults.standard.value(forKey: GlobalConstants.UserDefaultKeys.emailKey) as? String, let currentUserName = UserDefaults.standard.value(forKey: GlobalConstants.UserDefaultKeys.nameKey) as? String  else {
            print("Error in appendMessage: Email key is nil in userdefaults.")
            completion(false)
            return
        }
        let safeCurrentUserEmail = DatabaseManager.getSafeEmail(with: currentUserEmail)
        //Add/append the new message to the messages collection in the collectionID root node.
        database.child("\(existingConversationId)/messages").observeSingleEvent(of: .value) { [weak self] snapshot in
            guard var convoMessages = snapshot.value as? [[String: Any]] else {
                completion(false)
                return
            }
            var message = ""
            switch newMessage.kind {
            case .text(let text):
                message = text
            case .attributedText(_):
                break
            case .photo(let mediaItem):
                if let imageUrlString = mediaItem.url?.absoluteString {
                    message = imageUrlString
                }
            case .video(let mediaItem):
                if let videoUrlString = mediaItem.url?.absoluteString {
                    message = videoUrlString
                }
            case .location(let locationData):
                //The order matters here as we will be rendering the location with the longitude first, followed by the latitude.
                message = "\(locationData.location.coordinate.longitude),\(locationData.location.coordinate.latitude)"
                break
            case .emoji(_):
                break
            case .audio(_):
                break
            case .contact(_):
                break
            case .custom(_):
                break
            }
            let sentDateString = ChatViewController.dateFormatter.string(from: newMessage.sentDate)
            let newMessageDictionaryData: [String: Any] = [
                "id": newMessage.messageId,
                "receiver_name": recipientName,
                "type": newMessage.kind.stringValue,
                "content": message,
                "date": sentDateString,
                "sender_email": safeCurrentUserEmail,
                "is_read": false
            ]
            convoMessages.append(newMessageDictionaryData)
            let newLatestMessage: [String: Any] = [
                "date": sentDateString,
                "is_read": false,
                "message": message
            ]
            self?.database.child("\(existingConversationId)/messages").setValue(convoMessages) { error, _ in
                guard error == nil else {
                    print("Error in appendMessage: Error overriding the messages collection in the conversationID root node.")
                    completion(false)
                    return
                }
                //Update sender's latest message
                self?.database.child("\(safeCurrentUserEmail)/conversations").observeSingleEvent(of: .value, with: { snapshot in
                    let newConversationDataForCurrentUser: [String: Any] = [
                        "id": existingConversationId,
                        "other_user_email": recipientEmail,
                        "other_user_name": recipientName,
                        "latest_message": newLatestMessage,
                        "created_at": ChatViewController.dateFormatter.string(from: Date())
                    ]
                    var updatedCurrentUserConversations = [[String: Any]]()
                    if var currentUserConversations = snapshot.value as? [[String: Any]] {
                        var convoExistsInTheUsersConvoCollection = false
                        for (index, conversationDictionary) in currentUserConversations.enumerated() {
                            if let conversationId = conversationDictionary["id"] as? String, conversationId == existingConversationId {
                                convoExistsInTheUsersConvoCollection = true
                                var mutableConversationDictionary = conversationDictionary
                                mutableConversationDictionary["latest_message"] = newLatestMessage
                                currentUserConversations[index] = mutableConversationDictionary
                                break
                            }
                        }
                        //If the user has deleted the convo with the recipient previously and the user has other conversations with other users but not with the recipient, and then the user sends a message to the recipient. So, here we will need to create a new conversation collection for the logged in user along with the new convo with the recipient as the convo with the recipient will be nil as it has been deleted before.
                        if !convoExistsInTheUsersConvoCollection {
                            currentUserConversations.append(newConversationDataForCurrentUser)
                        }
                        updatedCurrentUserConversations = currentUserConversations
                    } else {
                        //User does not have a conversations collection, so we will create a new convo collection that will have the new convo with the recipient.
                        //The logged in user's conversation will be nil if the user had a single convo in his conversations collection, deleted that convo which resulted in the conversations collection being removed from the user's dictionary in the database and the logged in user sent a new message to the recipient. So, here we will need to create a new conversation collection for the logged in user along with the new convo with the recipient.
//                        print("Error in appendMessage: Error fetching the current user's conversations.")
                        updatedCurrentUserConversations = [newConversationDataForCurrentUser]
                    }
                    self?.database.child("\(safeCurrentUserEmail)/conversations").setValue(updatedCurrentUserConversations, withCompletionBlock: { error, _ in
                        guard error == nil else {
                            print("Error in appendMessage: Error overriding the current user's conversations collection that contains the new latest_message dictionary.")
                            completion(false)
                            return
                        }
                        //Update recipient's latest message
                        self?.database.child("\(recipientEmail)/conversations").observeSingleEvent(of: .value, with: { snapshot in
                            let newConversationDataForTheRecipient: [String: Any] = [
                                "id": existingConversationId,
                                "other_user_email": safeCurrentUserEmail,
                                "other_user_name": currentUserName,
                                "latest_message": newLatestMessage,
                                "created_at": ChatViewController.dateFormatter.string(from: Date())
                            ]
                            var updatedRecipientConversations = [[String: Any]]()
                            if var recipientConversations = snapshot.value as? [[String: Any]] {
                                var convoExistsInTheRecipientConvoCollection = false
                                for (index, conversationDictionary) in recipientConversations.enumerated() {
                                    if let conversationId = conversationDictionary["id"] as? String, conversationId == existingConversationId {
                                        convoExistsInTheRecipientConvoCollection = true
                                        var mutableConversationDictionary = conversationDictionary
                                        mutableConversationDictionary["latest_message"] = newLatestMessage
                                        recipientConversations[index] = mutableConversationDictionary
                                        break
                                    }
                                }
                                if !convoExistsInTheRecipientConvoCollection {
                                    recipientConversations.append(newConversationDataForTheRecipient)
                                }
                                updatedRecipientConversations = recipientConversations
                            } else {
                                updatedRecipientConversations = [newConversationDataForTheRecipient]
                            }
                            self?.database.child("\(recipientEmail)/conversations").setValue(updatedRecipientConversations, withCompletionBlock: { error, _ in
                                guard error == nil else {
                                    print("Error in appendMessage: Error overriding the recipient's conversations collection that contains the new latest_message dictionary.")
                                    completion(false)
                                    return
                                }
                                completion(true)
                            })
                        })
                    })
                })
            }
        }
    }
        
    /// Fetches and returns all of the user's conversations with the user's email.
    /// Parameters
    /// - `userEmail`: The user's email.
    /// - `completion`: Async closure to return with result.
    public func getAllConversationsForUser(with userEmail: String,
                                           completion: @escaping (Result<[Conversation], Error>) -> ()) {
        //Attaching a listener to the target user's conversation collection. NOTE: Up until now, we have only been using observeSingleEvent method of Firebase which only listens once as we did not need to listen continuously. Now, we will use the observe method which will observe/listen continuously for changes in the key that we are obeserving/listening to. So, every time a new conversation is added to the conversations collection, this completion/closure is called.
        let conversationsRef = database.child("\(userEmail)/conversations")
        observers.append(conversationsRef)
        conversationsRef.observe(.value) { snapshot in
            guard let value = snapshot.value as? [[String: Any]] else {
                completion(.failure(DatabaseError.failedToFetch))
                return
            }
            //CompactMap returns no nil values if there are any whereas Map can return a nil value if the value is nil. So, the conversations array below will not contain any conversations that have nil values. If any of the value in the guard let is nil, then that conversation object will be nil and won't be in the conversations array.
            let conversations: [Conversation] = value.compactMap { conversationDictionary in
                guard let conversationId = conversationDictionary["id"] as? String,
                      let recipientEmail = conversationDictionary["other_user_email"] as? String,
                      let recipientName = conversationDictionary["other_user_name"] as? String,
                      let latestMessage = conversationDictionary["latest_message"] as? [String: Any],
                      let convoCreationDate = conversationDictionary["created_at"] as? String,
                      let sentDate = latestMessage["date"] as? String,
                      let messageIsRead = latestMessage["is_read"] as? Bool,
                      let message = latestMessage["message"] as? String else {
                          return nil
                      }
                let latestMessageObject = LatestMessage(date: sentDate,
                                                        text: message,
                                                        isRead: messageIsRead)
                return Conversation(id: conversationId,
                                    otherUserName: recipientName,
                                    otherUserEmail: recipientEmail,
                                    latestMessage: latestMessageObject,
                                    creationDate: convoCreationDate)
            }
            completion(.success(conversations)) 
        }
    }
    
    /// Fetches and returns all of the messages for a conversation for the user
    /// with the conversation id.
    /// Parameters
    /// - `conversationId`: The conversation ID.
    /// - `completion`: Async closure to return with result.
    public func getAllMessagesForAConversation(with conversationId: String,
                                               completion: @escaping (Result<[Message], Error>) -> Void) {
        let messagesRef = database.child("\(conversationId)/messages")
        observers.append(messagesRef)
        messagesRef.observe(.value) { [weak self] snapshot in
            guard let self = self, let value = snapshot.value as? [[String: Any]] else {
                completion(.failure(DatabaseError.failedToFetch))
                return
            }
            let messages: [Message] = value.compactMap { messageDictionary in
                guard let messageId = messageDictionary["id"] as? String,
                      let senderEmail = messageDictionary["sender_email"] as? String,
                      let recipientName = messageDictionary["receiver_name"] as? String,
                      let messageType = messageDictionary["type"] as? String,
                      let isRead = messageDictionary["is_read"] as? Bool,
                      let messageContent = messageDictionary["content"] as? String,
                      let sentDate = messageDictionary["date"] as? String,
                      let date = ChatViewController.dateFormatter.date(from: sentDate) else {
                          return nil
                      }
                let sender = Sender(photoURL: "",
                                    senderId: senderEmail,
                                    displayName: recipientName)
                var messageKind: MessageKind?
                if messageType == "photo" {
                    messageKind = self.getMessageKindForMediaKind(.photo, messageContentUrl: messageContent)
                } else if messageType == "video" {
                    messageKind = self.getMessageKindForMediaKind(.video, messageContentUrl: messageContent)
                } else if messageType == "location" {
                    messageKind = self.getLocationMediaKind(with: messageContent)
                } else {
                    messageKind = .text(messageContent)
                }
                guard let messageKind = messageKind else { return nil }
                return Message(sender: sender,
                               messageId: messageId,
                               sentDate: date,
                               kind: messageKind)
            }
            completion(.success(messages))
        }
    }
    
    private func getMessageKindForMediaKind(_ mediaType: MediaContentType, messageContentUrl: String) -> MessageKind? {
        guard let contentUrl = URL(string: messageContentUrl),
              let placeholderImage = mediaType == .photo ? UIImage(systemName: "photo") : UIImage(systemName: "play.circle.fill") else { return nil }
        //A constant to render the image/video size to be displayed on the chat. NOTE: Width should be within device's width.
        let contentRenderingSize = CGSize(width: 300, height: 300)
        let media = Media(url: contentUrl,
                          image: nil,
                          placeholderImage: placeholderImage,
                          size: contentRenderingSize)
        return mediaType == .photo ? .photo(media) : .video(media)
    }
    
    private func getLocationMediaKind(with messageContent: String) -> MessageKind? {
        let locationComponents = messageContent.components(separatedBy: ",")
        guard let longitude = Double(locationComponents[0]),
              let latitude = Double(locationComponents[1]) else {
                  return nil
              }
        let location = Location(location: CLLocation(latitude: latitude,
                                                     longitude: longitude),
                                size: CGSize(width: 300, height: 300))
        return .location(location)
    }
    
    /// Deletes a conversation for the gived conversation ID in Firebase database for the currently logged in user.
    /// Parameters
    /// - `conversationId`: The conversation ID.
    /// - `completion`: Async closure to return with result.
    public func deleteAConversation(with conversationId: String,
                                    completion: @escaping (Bool) -> Void) {
        guard let loggedInUserEmail = UserDefaults.standard.value(forKey: GlobalConstants.UserDefaultKeys.emailKey) as? String else {
            print("Error in deleteAConversation: Email key is nil in userdefaults.")
            completion(false)
            return
        }
        let loggedInUserSafeEmail = DatabaseManager.getSafeEmail(with: loggedInUserEmail)
        print("Deleting a conversation with the id: \(conversationId).")
        //Get all of the user's conversations
        let userConvoReference = database.child("\(loggedInUserSafeEmail)/conversations")
        userConvoReference.observeSingleEvent(of: .value) { snapshot in
            if var userConvos = snapshot.value as? [[String: Any]] {
                var convoIndexToRemove = 0
                for conversation in userConvos {
                    if let convoId = conversation["id"] as? String,
                       convoId == conversationId {
                        print("Got the conversation with the id: \(conversationId) to delete.")
                        break
                    }
                    convoIndexToRemove += 1
                }
                userConvos.remove(at: convoIndexToRemove)
                userConvoReference.setValue(userConvos) { error, firebaseDatabaseRef in
                    guard error == nil else {
                        print("Error in deleteAConversation: Failed to set the new conversations value in the Firebase database.")
                        completion(false)
                        return
                    }
                    print("Deleted the conversation with the id: \(conversationId).")
                    completion(true)
                }
            } else {
                print("Error in deleteAConversation: Firebase snapshot value is nil.")
                completion(false)
                return
            }
        }
        //Delete the conversation with the conversation id in the user's conversation collection.
        //Reset the conversations for the user in the firebase database.
    }
    
    /// Checks if a conversation already exist in the database with the recipient's email. This is useful in the case where the currently logged in user has deleted a conversations with the recipient A and tries to create a new conversations with the recipient A. This ensures that there are no new conversations being created when the currenty logged in user who deleted the convo sends a new message. Else, the recipient A will have 2 conversations being displayed in his conversations page.
    /// Parameters
    /// - `recipientEmail`: The recipient's email.
    /// - `completion`: Async closure to return with result.
    public func checkIfConversationAlreadyExists(with recipientEmail: String,
                                                 completion: @escaping (Result<Conversation, DatabaseError>) -> Void) {
        let safeRecipientEmail = DatabaseManager.getSafeEmail(with: recipientEmail)
        guard let loggedInUserEmail = UserDefaults.standard.value(forKey: GlobalConstants.UserDefaultKeys.emailKey) as? String else {
            print("Error in deleteAConversation: Email key is nil in userdefaults.")
            completion(.failure(.emailIsNilInUserDefaults))
            return
        }
        let safeLoggedInUserEmail = DatabaseManager.getSafeEmail(with: loggedInUserEmail)
        database.child("\(safeRecipientEmail)/conversations").observeSingleEvent(of: .value) { snapshot in
            guard let conversations = snapshot.value as? [[String: Any]] else {
                completion(.failure(.failedToFetch))
                return
            }
            //Iterate and find the current user email in the recipient's conversations
            if let conversationDictionary = conversations.first(where: {
                guard let safeLoggedInUserEmailInRecipientConvos = $0["other_user_email"] as? String else { return false }
                return safeLoggedInUserEmailInRecipientConvos == safeLoggedInUserEmail
            }) {
                guard let conversationId = conversationDictionary["id"] as? String,
                      let recipientEmail = conversationDictionary["other_user_email"] as? String,
                      let recipientName = conversationDictionary["other_user_name"] as? String,
                      let latestMessage = conversationDictionary["latest_message"] as? [String: Any],
                      let convoCreationDate = conversationDictionary["created_at"] as? String,
                      let sentDate = latestMessage["date"] as? String,
                      let messageIsRead = latestMessage["is_read"] as? Bool,
                      let message = latestMessage["message"] as? String else {
                          completion(.failure(.conversationDoesNotExist))
                          return
                      }
                let latestMessageObject = LatestMessage(date: sentDate,
                                                        text: message,
                                                        isRead: messageIsRead)
                let conversation = Conversation(id: conversationId,
                                    otherUserName: recipientName,
                                    otherUserEmail: recipientEmail,
                                    latestMessage: latestMessageObject,
                                    creationDate: convoCreationDate)
                completion(.success(conversation))
                return
            }
            completion(.failure(.failedToFetch))
            return
        }
    }
    
}

/*
    Database overall schema: User's safe email and users database are the root nodes.
    Note: "users", "conversations", and "conversation_id" will only exist after their creation.
    We only create a new conversation when a message has been sent for that conversation.
    A conversation will also be a root node with it's id as it's key. (So we will have many conversation root nodes). This is done so that we can observe the conversation directly as a root node. If this root node was a sub-node in a user root node, then we would have to query through different user root nodes to observe it and it will not be efficient.
    
    "userSafeEmail": {
        "first_name": String,
        "last_name": String,
        "conversations": [
            0: {
                "id": String,
                "other_user_email": String,
                "latest_message": {
                    "date": Date,
                    "message": String/Video/Image,
                    "is_read": Bool
                }
            }
        ]
    }
    "conversation_id": {
        "messages": [
            0: {
                "id": String,
                "type": String/Video/Image,
                "content": String/Video/Image,
                "date": Date,
                "sender_email": String,
                "is_read": Bool
            }
        ]
    }
    "users": [
        0: {
            "email": String,
            "name": String
        },
        1: {
            "email": String,
            "name": String
        }
    ]
 
*/
