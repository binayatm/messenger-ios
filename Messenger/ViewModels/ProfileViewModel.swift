//
//  ProfileViewModel.swift
//  Messenger
//
//  Created by Binaya on 21/02/2023.
//

import UIKit

enum ProfileSections {
    
    case info,
         logout
    
}

struct ProfileViewModel {
    
    let profileSections: ProfileSections
    let title: String
    let handler: (() -> Void)?
    
}
