//
//  ConversationTableViewCell.swift
//  Messenger
//
//  Created by Binaya on 05/10/2022.
//

import UIKit
import SDWebImage

class ConversationTableViewCell: UITableViewCell {
    
    // MARK: - Static properties
    static let identifier = "ConversationTableViewCell"
    
    // MARK: - Properties
    private let userImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.layer.cornerRadius = 30
        imageView.layer.masksToBounds = true
        return imageView
    }()
    private let userNameLabel: UILabel = {
        let label = UILabel()
        label.font = .systemFont(ofSize: 18, weight: .semibold)
        return label
    }()
    private let userMessageLabel: UILabel = {
        let label = UILabel()
        label.font = .systemFont(ofSize: 14 , weight: .regular)
        label.numberOfLines = 0
        return label
    }()
    
    // MARK: - Lifecycle methods
    //Initializer/Constructor
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setup()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        setupSubViews()
    }
    
    public func configure(with model: Conversation) {
        //We can get the other user's profile picture URL easily as we've used the safe email in the path with a standard naming convention.
        let otherUserImagePath = "images/\(model.otherUserEmail)_profile_picture.png"
        StorageManager.shared.getProfilePictureDownloadUrl(for: otherUserImagePath) { [weak self] result in
            switch result {
            case .success(let url):
                DispatchQueue.main.async {
                    self?.userImageView.sd_setImage(with: url, completed: nil)
                }
            case .failure(let error):
                print("Failed to get \(model.otherUserName) user's profile picture download URL: \(error)")
            }
        }
        userNameLabel.text = model.otherUserName
        userMessageLabel.text = model.latestMessage.text
    }
    
    // MARK: - Methods
    private func setup() {
        contentView.addSubview(userImageView)
        contentView.addSubview(userNameLabel)
        contentView.addSubview(userMessageLabel)
    }
    
    private func setupSubViews() {
        userImageView.frame = CGRect(x: 10,
                                     y: 10,
                                     width: 60,
                                     height: 60)
        userNameLabel.frame = CGRect(x: userImageView.right + 10,
                                     y: 10,
                                     width: contentView.width - 20 - userImageView.width,
                                     height: (contentView.height - 20) / 2)
        userMessageLabel.frame = CGRect(x: userImageView.right + 10,
                                        y: userNameLabel.bottom + 10,
                                     width: contentView.width - 20 - userImageView.width,
                                     height: (contentView.height - 20) / 2)
    }
    
}
