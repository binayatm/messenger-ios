//
//  NewConversationTableViewCell.swift
//  Messenger
//
//  Created by Binaya on 31/01/2023.
//

import UIKit
import SDWebImage

class NewConversationTableViewCell: UITableViewCell {
    
    // MARK: - Static properties
    static let identifier = "NewConversationTableViewCell"
    
    // MARK: - Properties
    private let userImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.layer.cornerRadius = 25
        imageView.layer.masksToBounds = true
        return imageView
    }()
    private let userNameLabel: UILabel = {
        let label = UILabel()
        label.font = .systemFont(ofSize: 16, weight: .semibold)
        return label
    }()
    var otherUser: SearchResultUser? {
        didSet {
            setOtherUserData()
        }
    }
    
    // MARK: - Lifecycle methods
    //Initializer/Constructor
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setup()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        setupSubViews()
    }
    
    public func setOtherUserData() {
        guard let otherUser = otherUser else { return }
        //We can get the other user's profile picture URL easily as we've used the safe email in the path with a standard naming convention.
        let otherUserImagePath = "images/\(otherUser.email)_profile_picture.png"
        StorageManager.shared.getProfilePictureDownloadUrl(for: otherUserImagePath) { [weak self] result in
            switch result {
            case .success(let url):
                DispatchQueue.main.async {
                    self?.userImageView.sd_setImage(with: url, completed: nil)
                }
            case .failure(let error):
                print("Failed to get \(otherUser.name) user's profile picture download URL: \(error)")
            }
        }
        userNameLabel.text = otherUser.name
    }
    
    // MARK: - Methods
    private func setup() {
        contentView.addSubview(userImageView)
        contentView.addSubview(userNameLabel)
    }
    
    private func setupSubViews() {
        userImageView.frame = CGRect(x: 10,
                                     y: 10,
                                     width: 50,
                                     height: 50)
        userNameLabel.frame = CGRect(x: userImageView.right + 10,
                                     y: 20,
                                     width: contentView.width - 20 - userImageView.width,
                                     height:  30)
    }
    
}
