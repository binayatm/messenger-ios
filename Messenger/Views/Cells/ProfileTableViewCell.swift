//
//  ProfileTableViewCell.swift
//  Messenger
//
//  Created by Binaya on 21/02/2023.
//

import UIKit

class ProfileTableViewCell: UITableViewCell {

    // MARK: - Static properties
    static let identifier = "ProfileTableViewCell"
    
    // MARK: - Lifecyele methods
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        textLabel?.text = ""
        textLabel?.textColor = .label
        textLabel?.textAlignment = .left
        selectionStyle = .default
    }

    // MARK: - Other methods
    func setup(with viewModel: ProfileViewModel) {
        textLabel?.text = viewModel.title
        switch viewModel.profileSections {
        case .info:
            textLabel?.textColor = .label
            textLabel?.font = .systemFont(ofSize: 18)
            selectionStyle = .none
        case .logout:
            textLabel?.textColor = .red
            textLabel?.textAlignment = .center
        }
    }
    
}
