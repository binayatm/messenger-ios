# Messenger Real-Time Chat App. 

A beginners swift project to create a real-time chat application in Swift 5 using Firebase.

## Features
- Facebook Login
- Google Login
- Email and Password Registration/Login
- Text Messages
- Photo Messages
- Video Messages
- Location Messages
- Real-Time Conversations
- Search for Users
- Delete Conversations
- User Profile
- Dark Mode Support
